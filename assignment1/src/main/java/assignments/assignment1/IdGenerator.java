package assignments.assignment1;

import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;


public class IdGenerator {
    static HashMap<Character, Integer> charToValue = new HashMap<>(36);
    static char[] valueToChar = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    /*
     * Code pada method main tidak boleh diganti sama sekali!
     */
    public static void main(String[] args) {
        buildMapCharToValue();
        Scanner input = new Scanner(System.in);
        System.out.println("Selamat Datang di Perpustakaan!");

        boolean shouldTerminateProgram = false;

        while (!shouldTerminateProgram) {
            printMenu();
            System.out.print("Menu yang anda pilih: ");
            int menu = input.nextInt();
            input.nextLine();

            if (menu == 1) {
                System.out.print("Program Studi: ");
                String programStudi = input.nextLine();
                System.out.print("Angkatan: ");
                String angkatan = input.nextLine();
                System.out.print("Tanggal Lahir: ");
                String tanggalLahir = input.nextLine();

                System.out.println(generateId(programStudi, angkatan, tanggalLahir));
            } else if (menu == 2) {
                System.out.print("ID Anggota: ");
                String idAnggota = input.nextLine();

                System.out.print("Validitas: ");
                System.out.println(checkValidity(idAnggota));
            } else {
                shouldTerminateProgram = true;
                System.out.println("Sampai jumpa!");
            }
        }

        input.close();
    }

    /*
     * Method buildMapCodeToNumber adalah method untuk membuat mapping reference numbers Code 93
     */
    public static void buildMapCharToValue() {
        for (int count = 0; count < valueToChar.length; count++) {
            charToValue.put(valueToChar[count], count);
        }
    }

    /*
     * Method getCharFromValue adalah method yang akan mengembalikan Code 93 dari value yang diberikan
     */
    private static char getCharFromValue(int value) {
        return valueToChar[value];
    }

    /*
     * Method getValueFromChar adalah method yang akan mengembalikan value dari Code 93 yang diberikan
     */
    private static int getValueFromChar(char character) {
        return charToValue.get(character);
    }

    /**
     * Method getChecksumC adalah method untuk generate checksum C sesuai ketentuan
     * Parameter berupa 11 karakter ID
     */
    public static char getChecksumC(String tempId) {
        int jumlahHasilKali = 0;
        for (int i = 0; i < 3; i++) { 
            int value = getValueFromChar(tempId.charAt(i));
            jumlahHasilKali += (value * (11 - i));
        }
        for (int i = 3; i < 11; i++) {
            jumlahHasilKali += (getValueFromChar(tempId.charAt(i)) * (11 - i));

        }
        char result = getCharFromValue(jumlahHasilKali%36);
        return result;
    }

    /**
     * Method getChecksumK adalah method untuk generate checksum K sesuai ketentuan
     * Parameter berupa 12 karakter ID
     */
    public static char getChecksumK(String tempId) {
        int jumlahHasilKali = 0;
        for (int i = 0; i < 3; i++) {
            int value = getValueFromChar(tempId.charAt(i));
            jumlahHasilKali += (value * (12 - i));
        }
        for (int i = 3; i < 12; i++) {
            jumlahHasilKali += (getValueFromChar(tempId.charAt(i)) * (12 - i));
        }

        char result = getCharFromValue(jumlahHasilKali%36);
        return result;
    }

    private static void printMenu() {
        System.out.println("--------------------------------------------");
        System.out.println("Menu yang tersedia:");
        System.out.println("1. Generate ID anggota untuk anggota baru");
        System.out.println("2. Check validitas ID anggota");
        System.out.println("3. Keluar");
        System.out.println("--------------------------------------------");
    }

    /*
     * Method generateId adalah method untuk membuat ID keanggotaan perpustakaan
     */
    public static String generateId(String programStudi, String angkatan, String tanggalLahir){
        String[] validProdi = new String[] {"SIK", "SSI", "MIK", "MTI", "DIK"};

        // Validasi input sesuai ketentuan
        try {
            boolean found = false;
            for (String prodi : validProdi) {
                if (prodi.equals(programStudi)) {
                    found = true;
                    break;
                } 
            }
            if (found == false) {
                throw new IOException();
            }
            if (angkatan.length() != 4 || Integer.parseInt(angkatan) < 2000 || Integer.parseInt(angkatan) > 2021) {
                throw new IOException();
            }
            if (tanggalLahir.length() != 10 || tanggalLahir.indexOf("/") != 2 || tanggalLahir.indexOf("/", 3) != 5) {
                throw new IOException();
            } 

        } catch (Exception e) {
            return "Input tidak valid!";
        }

        // Menyusun id anggota
        String idAnggota = "";
        idAnggota += programStudi;
        idAnggota += angkatan.substring(2);
        idAnggota += tanggalLahir.substring(0, 5).replace("/", "");
        idAnggota += tanggalLahir.substring(8);
        
        char checksumC = getChecksumC(idAnggota);
        idAnggota += checksumC;

        char checksumK = getChecksumK(idAnggota);
        idAnggota += checksumK;

        return "ID Anggota: " + idAnggota;
    }

    /*
     * Method checkValidity adalah method untuk mengecek validitas ID keanggotaan perpustakaan
     * Parameter dan return type dari method ini tidak boleh diganti
     */
    public static boolean checkValidity(String idAnggota) {
        try {
            // Validasi panjang id
            if (idAnggota.length() != 13) {
                throw new IOException();
            }

            for (int i = 0; i < 11; i++) {
                // Validasi 3 karakter pertama adalah UpperCase
                if (i < 3) {
                    boolean isUpperCase = Character.isUpperCase(idAnggota.charAt(i));
                    if (!isUpperCase) {
                        throw new IOException();
                    }
                // Validasi karakter 4-11 adalah numerik
                } else {
                    Boolean isNumeric = Character.isDigit(idAnggota.charAt(i));
                    if (!isNumeric) {
                        throw new IOException();
                    }
                }
            }

            // Validasi checksum C dan K
            String tempId = idAnggota.substring(0, 11);
            char checksumC = getChecksumC(tempId);
            if (checksumC != idAnggota.charAt(11)) {
                throw new IOException();
            }

            tempId += checksumC;
            char checksumK = getChecksumK(tempId);
            if (checksumK != idAnggota.charAt(12)) {
                throw new IOException();
            }
            return true;

        } catch (Exception e) {
            return false;
        }
    }
}
