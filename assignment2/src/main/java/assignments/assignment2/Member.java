package assignments.assignment2;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Member {
    // Atribut Class Member
    private String id;
    private String name;
    private String dateOfBirth;
    private String studyProgram;
    private String angkatan;
    private long fine;
    private int point;
    private BookLoan[] bookLoans;

    // Constructor
    public Member(String id, String name, String dateOfBirth, String studyProgram, String angkatan, long fine, int point) {
        this.id = id;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.studyProgram = studyProgram;
        this.angkatan = angkatan;
        this.fine = fine;
        this.point = point;
    }

    @Override
    public String toString() {
        return  "ID Anggota: " + id + 
                "\nNama Anggota: " + name +
                "\nTotal Point: " + point +
                "\nDenda: " + fine;
    }

    /* 
     * Method pinjam untuk menambahkan Object BookLoan ke dalam Array BookLoan dan mengurangi Stock buku
     */
    public void pinjam(Book book, String loanDate) {

        if (bookLoans == null) {
            bookLoans = new BookLoan[1];
            bookLoans[bookLoans.length-1] = new BookLoan(this, book, loanDate, Long.parseLong("0") , true);
        } else {
            BookLoan[] newBookLoans = new BookLoan[bookLoans.length + 1];
            for (int i = 0; i < bookLoans.length; i++){
                newBookLoans[i] = bookLoans[i];
            }
            newBookLoans[newBookLoans.length -1] = new BookLoan(this, book, loanDate, Long.parseLong("0") , true);
            bookLoans = newBookLoans.clone();
        }
        book.setStock(book.getStock()-1);
        
    }

    /* 
     * Method kembali untuk mengubah status peminjaman buku pada BookLoan menjadi false, 
     * menambahkan stock buku
     * menambahkan point anggota sesuai kategori buku
     * mengoperasikan perhitungan denda sesuai durasi peminjaman
     */
    public void kembali(Book book, String returnDate) throws Exception {
        
        if (bookLoans != null) {
            for (int i = 0; i < bookLoans.length; i++) {
                if(bookLoans[i].getBook().equals(book)) {
                    bookLoans[i].setStatus(false);
                    book.setStock(book.getStock() + 1);
                    this.point += book.getCategory().getPoint();
    
                    bookLoans[i].setReturnDate(returnDate);
                    Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(bookLoans[i].getLoanDate()); 
                    Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(bookLoans[i].getReturnDate()); 
                
                    long diffInMillies = date2.getTime() - date1.getTime();
                    long diffDate = TimeUnit.DAYS.convert(diffInMillies,TimeUnit.MILLISECONDS);
    
                    if (diffDate >= 7) {
                        this.setFine(this.getFine() + (diffDate-7)*BookLoan.getDendaPerHari());
                        bookLoans[i].setFine((diffDate-7));
    
                    }
                }
            }
        }
        
    }

    /* 
     * Method detail untuk mencetak detail Anggota dan riwayat peminjaman buku
     */
    public void detail() {
        System.out.println(this.toString());
        System.out.println("Riwayat Peminjaman Buku :");
        if (bookLoans != null) {
            for (int i = 0; i < bookLoans.length; i++) {
                System.out.println("—------------- " + (i + 1) + " —-------------");
                System.out.println("Judul Buku: " + bookLoans[i].getBook().getTitle());
                System.out.println("Penulis Buku: " + bookLoans[i].getBook().getAuthor());
                System.out.println("Penerbit Buku: " + bookLoans[i].getBook().getPublisher());
                System.out.println("Kategori: " + bookLoans[i].getBook().getCategory().getCategoryName());
                System.out.println("Point: " + bookLoans[i].getBook().getCategory().getPoint());
                System.out.println("Tanggal Peminjaman: " + bookLoans[i].getLoanDate());
                System.out.println("Tanggal Pengembalian: " + bookLoans[i].getReturnDate());
                System.out.println("Denda: Rp " + bookLoans[i].getFine()); 
            }
        }

    }

    /* 
     * Method bayarDenda untuk mengurangi total denda Anggota sesuai jumlah yang dibayarkan
     */
    public void bayarDenda(long amount) {
        if (amount >= this.getFine()) {
            this.setFine(0);
        } else {
            this.setFine(this.getFine() - amount);
        }
    }

    /* 
     * Method getDateDiff untuk mendapatkan lama durasi antara dua tanggal
     */
    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }

    // Getter dan Setter
    public String getId() {
        return this.id;
    }
    public BookLoan[] getBookLoans() {
        return this.bookLoans;
    }

    public int getJumlahPinjam() {
        int jumlahPinjam = 0;
        if (bookLoans != null) {
            for (int i = 0; i < bookLoans.length; i++) {
                if (bookLoans[i].getStatus()) {
                    jumlahPinjam++;
                }
            }
        }
        return jumlahPinjam;
    }

    public long getFine() {
        return this.fine;
    }

    public String getName() {
        return this.name;
    }

    public int getPoint() {
        return this.point;
    }

    public void setFine(long fine) {
        this.fine = fine;
    }
    
}
