package assignments.assignment2;


public class Category {
    // Atribut Class Category
    private String namaKategori;
    private int point;

    // Constructor
    public Category(String namaKategori, int point) {
        this.namaKategori = namaKategori;
        this.point = point;
    }

    @Override
    public String toString() {
        return "Kategori " + namaKategori + " dengan " + point + " point berhasil ditambahkan";
    }

    // Method Getter
    public String getCategoryName() {
        return this.namaKategori;
    }

    public int getPoint() {
        return this.point;
    }
}
