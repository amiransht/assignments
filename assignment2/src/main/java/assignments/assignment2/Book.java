package assignments.assignment2;

public class Book {
    //Atribut Class Book
    private String title;
    private String author;
    private String publisher;
    private int stock;
    private Category category;

    // Constructor
    public Book(String title, String author, String publisher, int stock, Category category) {
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.stock = stock;
        this.category = category;
    }

    @Override
    public String toString() {
        return "Buku " + title + " oleh " + author + " berhasil ditambahkan";
    }


    // Getter and Setter
    public String getTitle() {
        return this.title;
    }

    public String getAuthor() {
        return this.author;
    }

    public Category getCategory() {
        return this.category;
    }

    public int getStock() {
        return this.stock;
    }

    public String getPublisher() {
        return this.publisher;
    }

    public void setStock(int newStock) {
        this.stock = newStock;
    }
}
