package assignments.assignment2;

 
public class BookLoan {
    private static long DENDA_PER_HARI;
    private Member member;
    private Book book;
    private String loanDate;
    private String returnDate = "-";
    private long fine;
    private boolean status;

    public BookLoan(Member member, Book book, String loanDate, long fine, boolean status) {
        this.member = member;
        this.book = book;
        this.loanDate = loanDate;
        this.fine = fine;
        this.status = status;
        BookLoan.DENDA_PER_HARI = 3000;
    }

    public boolean getStatus() {
        return this.status;
    }

    public String getReturnDate() {
        return this.returnDate;
    }

    public String getLoanDate() {
        return this.loanDate;
    }

    public Book getBook() {
        return this.book;
    }

    public static long getDendaPerHari() {
        return DENDA_PER_HARI;
    }

    public long getFine() {
        return this.fine;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setFine(long fine) {
        this.fine = fine * DENDA_PER_HARI;

    }

}
