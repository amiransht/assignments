package assignments.assignment2;

import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class Library {
    private Scanner input = new Scanner(System.in);

    // Atribut Class Library
    private Member[] members;
    private Book[] books;
    private Category[] categories;
    private static HashMap<Character, Integer> charToValue = new HashMap<>(36);
    private static char[] valueToChar = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    private int indexMember = 0;
    private int indexBook = 0;
    private int indexBookLoan = 0;
    private int indexCategory = 0;

    // Program Utama
    public static void main(String[] args) {
        Library program = new Library();
        program.menu();
    }

    /**
     * Method getChecksumC adalah method untuk generate checksum C sesuai ketentuan
     * Parameter berupa 11 karakter ID
     */
    public static char getChecksumC(String tempId) {
        int jumlahHasilKali = 0;
        for (int i = 0; i < 3; i++) { 
            int value = getValueFromChar(tempId.charAt(i));
            jumlahHasilKali += (value * (11 - i));
        }
        for (int i = 3; i < 11; i++) {
            jumlahHasilKali += (getValueFromChar(tempId.charAt(i)) * (11 - i));

        }
        char result = getCharFromValue(jumlahHasilKali%36);
        return result;
    }

    /**
     * Method getChecksumK adalah method untuk generate checksum K sesuai ketentuan
     * Parameter berupa 12 karakter ID
     */
    public static char getChecksumK(String tempId) {
        int jumlahHasilKali = 0;
        for (int i = 0; i < 3; i++) {
            int value = getValueFromChar(tempId.charAt(i));
            jumlahHasilKali += (value * (12 - i));
        }
        for (int i = 3; i < 12; i++) {
            jumlahHasilKali += (getValueFromChar(tempId.charAt(i)) * (12 - i));
        }

        char result = getCharFromValue(jumlahHasilKali%36);
        return result;
    }

    /*
     * Method buildMapCodeToNumber adalah method untuk membuat mapping reference numbers Code 93
     */
    public static void buildMapCharToValue() {
        for (int count = 0; count < valueToChar.length; count++) {
            charToValue.put(valueToChar[count], count);
        }
    }

    /*
     * Method getCharFromValue adalah method yang akan mengembalikan Code 93 dari value yang diberikan
     */
    private static char getCharFromValue(int value) {
        return valueToChar[value];
    }

    /*
     * Method getValueFromChar adalah method yang akan mengembalikan value dari Code 93 yang diberikan
     */
    private static int getValueFromChar(char character) {
        return charToValue.get(character);
    }

    /* 
     * Method untuk mencari kategori dalam Array Category, return true jika ditemukan
     */ 
    public boolean foundCategory(String kategori) {
        boolean foundCategory = false;
        if (this.categories != null) {
            for (int row = 0; row < categories.length; row++){
                if (categories[row].getCategoryName().toLowerCase().equals(kategori.toLowerCase())) {
                    foundCategory = true;
                    setIndexCategory(row);
                    break;
                }
            } 
        }
        return foundCategory; 
    }

    // Setter
    public void setIndexCategory(int indexCategory) {
        this.indexCategory = indexCategory;
    }

    /* 
     * Method untuk mencari ID dalam Array Member, return true jika ditemukan
     */ 
    public boolean foundID(String idAnggota) {
        boolean foundID = false;
        if (members != null) {
            for (int i = 0; i < members.length; i++) {
                if (members[i].getId().equals(idAnggota)) {
                    foundID = true;
                    setIndexMember(i);
                    break;
                }    
            }
        }
        return foundID;
    }

    // Setter
    public void setIndexMember(int indexMember) {
        this.indexMember = indexMember;
    }

    /* 
     * Method untuk mencari buku dalam Array Book, return true jika ditemukan
     */ 
    public boolean foundBook(String judul) {
        boolean foundBook = false;
        if (books != null) {
            for (int j = 0; j < books.length; j++) {
                if (books[j].getTitle().equals(judul)) {
                    foundBook = true;
                    setIndexBook(j);
                    break;
                } 
            }
        }
        return foundBook;
    }

    // Setter
    public void setIndexBook(int indexBook) {
        this.indexBook = indexBook;
    }

    /* 
     * Method untuk mencari buku yang dipinjam dalam Array BookLoan yang ada dalam Class Member, 
     * return true jika ditemukan
     */ 
    public boolean foundBookLoan(String judul, int indexMember) {
        boolean foundBookLoan = false;
        BookLoan[] daftarPinjam = members[indexMember].getBookLoans();
        if (daftarPinjam != null) {
            for (int k = 0; k < daftarPinjam.length; k++) {
                if (daftarPinjam[k].getBook().getTitle().equals(judul) && daftarPinjam[k].getStatus() == true) {
                    foundBookLoan = true;
                    setIndexBookLoan(k);
                    break;
                }
            }
        }
        return foundBookLoan;
    }

    // Setter
    public void setIndexBookLoan(int indexBookLoan) {
        this.indexBookLoan = indexBookLoan;
    }

    /* 
     * Method untuk menampilkan menu dan perintah
     */ 
    public void menu() {
        int command = 0;
        boolean hasChosenExit = false;
        System.out.println("Selamat Datang di Sistem Perpustakaan SistakaNG!");
        while (!hasChosenExit) {
            System.out.println("================ Menu Utama ================\n");
            System.out.println("1. Tambah Anggota");
            System.out.println("2. Tambah Kategori");
            System.out.println("3. Tambah Buku");
            System.out.println("4. Peminjaman");
            System.out.println("5. Pengembalian");
            System.out.println("6. Pembayaran Denda");
            System.out.println("7. Detail Anggota");
            System.out.println("8. 3 Peringkat Pertama");
            System.out.println("99. Keluar");
            System.out.print("Masukkan pilihan menu: ");
            command = Integer.parseInt(input.nextLine());
            System.out.println();

            // Program Tambah Anggota
            if (command == 1) {
                buildMapCharToValue();

                System.out.println("---------- Tambah Anggota ----------");
                System.out.print("Nama: ");
                String nama = input.nextLine();
                System.out.print("Program Studi (SIK/SSI/MIK/MTI/DIK): ");
                String programStudi = input.nextLine();
                System.out.print("Angkatan: ");
                String angkatan = input.nextLine();
                System.out.print("Tanggal Lahir (dd/mm/yyyy): ");
                String tanggalLahir = input.nextLine();
                
                String[] listProdi = new String[] {"SIK", "SSI", "MIK", "MTI", "DIK"};

                // Validasi input sesuai ketentuan
                try {
                    boolean found = false;
                    for (String prodi : listProdi) {
                        if (prodi.equals(programStudi)) {
                            found = true;
                            break;
                        } 
                    }
                    if (found == false) {
                        throw new IOException();
                    }
                    if (angkatan.length() != 4 || Integer.parseInt(angkatan) < 2000 || Integer.parseInt(angkatan) > 2021) {
                        throw new IOException();
                    }
                    if (tanggalLahir.length() != 10 || tanggalLahir.indexOf("/") != 2 || tanggalLahir.indexOf("/", 3) != 5) {
                        throw new IOException();
                    } 

                    // Menyusun id anggota
                    String idAnggota = "";
                    idAnggota += programStudi;
                    idAnggota += angkatan.substring(2);
                    idAnggota += tanggalLahir.substring(0, 5).replace("/", "");
                    idAnggota += tanggalLahir.substring(8);
                    
                    char checksumC = getChecksumC(idAnggota);
                    idAnggota += checksumC;
            
                    char checksumK = getChecksumK(idAnggota);
                    idAnggota += checksumK;

                    // Menambahkan Object Member ke dalam Array Member
                    if (this.members == null) {
                        this.members = new Member[1];
                        this.members[members.length-1] = new Member(idAnggota, nama, tanggalLahir, programStudi, angkatan, 0, 0);
                    } else {
                        Member[] newMembers = new Member[members.length + 1];

                        for (int row = 0; row < members.length; row++){
                            newMembers[row] = members[row];
                        } 

                        newMembers[newMembers.length -1] = new Member(idAnggota, nama, tanggalLahir, programStudi, angkatan, 0, 0);
                        members = newMembers.clone();
                    }

                    System.out.println("Member " + nama + " berhasil ditambahkan dengan data:");
                    System.out.println(members[members.length-1].toString());
        
                } catch (Exception e) {
                    System.out.println("Tidak dapat menambahkan anggota silahkan periksa kembali input anda!");
                }
            
            // Program Tambah Kategori
            } else if (command == 2) {
                System.out.println("---------- Tambah Kategori ----------");
                System.out.print("Nama Kategori: ");
                String namaKategori = input.nextLine();
                System.out.print("Point: ");
                String point = input.nextLine();

                // Menambahkan Object Category ke dalam Array Category
                if (this.categories == null) {
                    this.categories = new Category[1];
                    this.categories[categories.length -1] = new Category(namaKategori, Integer.parseInt(point));
                    System.out.println(categories[categories.length-1].toString());

                } else {

                    if (!foundCategory(namaKategori)){
                        Category[] newCategories = new Category[categories.length + 1];
                        for (int row = 0; row < categories.length; row++){
                            newCategories[row] = categories[row];
                        }

                        newCategories[newCategories.length -1] = new Category(namaKategori, Integer.parseInt(point));
                        categories = newCategories.clone();
                    
                        System.out.println(categories[categories.length-1].toString());
                    } else {
                        System.out.println("Kategori " + categories[indexCategory].getCategoryName() + " sudah pernah ditambahkan");
                    }
                }

            // Program Tambah Buku
            } else if (command == 3) {
                System.out.println("---------- Tambah Buku ----------");
                System.out.print("Judul: ");
                String judul = input.nextLine();
                System.out.print("Penulis: ");
                String penulis = input.nextLine();
                System.out.print("Penerbit: ");
                String penerbit = input.nextLine();
                System.out.print("Kategori: ");
                String kategori = input.nextLine();
                System.out.print("Stok: ");
                int stok = Integer.parseInt(input.nextLine());
                
                // Mencari buku dengan judul dan penulis yang sama dalam Array Book
                int index = 0;
                boolean foundBook = false;
                if (this.books != null) {
                    for(int row = 0; row < books.length; row++) {
                        if (books[row].getTitle().toLowerCase().equals(judul.toLowerCase()) &&
                            books[row].getAuthor().toLowerCase().equals(penulis.toLowerCase())) {
                                foundBook = true;
                                index = row;
                                break;
                            }
                    }
                }
                
                // Validasi
                if (!foundBook) {
                    if (foundCategory(kategori)) {
                        if (stok > 0) {
                            // Menambahkan Object Book ke dalam Array Book
                            if (this.books == null) {
                                this.books = new Book[1];
                                this.books[books.length -1] = new Book(judul, penulis, penerbit, stok, categories[indexCategory]);
                            } else {
                                Book[] newBooks = new Book[books.length + 1];
                                for (int row = 0; row < books.length; row++){
                                    newBooks[row] = books[row];
                                }
                                newBooks[newBooks.length -1] = new Book(judul, penulis, penerbit, stok, categories[indexCategory]);
                                books = newBooks.clone();
                            }
                            
                            System.out.println(books[books.length-1].toString());
                        } else {
                            System.out.println("Stok harus lebih dari 0");
                        }
                    } else {
                        System.out.println("Kategori " + kategori + " tidak ditemukan");
                    }
                } else {
                    System.out.println(books[index].getTitle() + " oleh " + books[index].getAuthor() + " sudah pernah ditambahkan");
                }
                
            // Program Peminjaman Buku
            } else if (command == 4) {
                System.out.println("---------- Peminjaman Buku ----------");
                System.out.print("ID Anggota: ");
                String id = input.nextLine();
                System.out.print("Judul Buku: ");
                String judul = input.nextLine();
                System.out.print("Penulis Buku: ");
                String penulis = input.nextLine();
                System.out.print("Tanggal Peminjaman: ");
                String tanggalPinjam = input.nextLine();
                
                // Validasi
                if (foundID(id)) {
                    if (foundBook(judul)) {
                        if (books[indexBook].getStock() >= 1) {
                            if (members[indexMember].getJumlahPinjam() <= 3) {
                                if (members[indexMember].getFine() < 5000) {
                                    if (!foundBookLoan(judul, indexMember)) {

                                        // Menjalankan method pinjam dari Class Member
                                        members[indexMember].pinjam(books[indexBook], tanggalPinjam);
                                        System.out.println(members[indexMember].getName() + " berhasil meminjam Buku " + judul + "!");

                                    } else {
                                        System.out.println(judul + " oleh " + penulis + " sedang dipinjam");
                                    }
                                } else {
                                    System.out.println("Denda lebih dari Rp 5000");
                                }
                            } else {
                                System.out.println("Jumlah buku yang sedang dipinjam sudah mencapai batas maksimal");
                            }
                        } else {
                            System.out.println(judul + " oleh " + penulis + " tidak tersedia");
                        }
                    } else {
                        System.out.println("Buku " + judul + " oleh " + penulis + " tidak ditemukan");
                    }
                } else {
                    System.out.println("Anggota dengan ID " + id + " tidak ditemukan");
                }
            
            // Program Pengembalian Buku
            } else if (command == 5) {
                System.out.println("---------- Pengembalian Buku ----------");
                System.out.print("ID Anggota: ");
                String id = input.nextLine();
                System.out.print("Judul Buku: ");
                String judul = input.nextLine();
                System.out.print("Penulis Buku: ");
                String penulis = input.nextLine();
                System.out.print("Tanggal Pengembalian: ");
                String tanggalKembali = input.nextLine();

               
                // Validasi
                if (foundID(id)) {
                    if (foundBook(judul)) {
                        System.out.println(indexMember);
                        if (foundBookLoan(judul, indexMember)) {
                            try {
                                BookLoan[] daftarPinjam = members[indexMember].getBookLoans();
                                // Menjalankan menthod kembali pada Class Member
                                members[indexMember].kembali(books[indexBook], tanggalKembali);
                                System.out.println("Buku " + judul + " berhasil dikembalikan oleh " + 
                                                    members[indexMember].getName() + " dengan denda Rp " + 
                                                    daftarPinjam[indexBookLoan].getFine() + "!");
                            } catch (Exception e) {
                                System.out.println("Input tanggal tidak valid");
                            }
                        } else {
                            System.out.println("Buku " + judul + " tidak sedang dipinjam" + indexMember);
                        }
                    } else {
                        System.out.println(judul + " oleh " + penulis + " tidak ditemukan");
                    }
                } else {
                    System.out.println("Anggota dengan ID "+ id + " tidak ditemukan");
                }
            
            // Program Pembayaran Denda
            } else if (command == 6) {
                System.out.println("---------- Pembayaran Denda ----------");
                System.out.print("ID Anggota: ");
                String id = input.nextLine();
                System.out.print("Jumlah: ");
                Long jumlah = Long.parseLong(input.nextLine());


                // Validasi
                if (foundID(id)) {
                    if (members[indexMember].getFine() != 0) {

                        // Menjalankan method bayarDenda pada Class Member
                        if (jumlah < members[indexMember].getFine()) {
                            members[indexMember].bayarDenda(jumlah);
                            System.out.println(members[indexMember].getName() + " berhasil membayar denda sebesar Rp " + jumlah);
                            System.out.println("Sisa denda saat ini: Rp " + members[indexMember].getFine());
                        } else {
                            long kembalian = jumlah - members[indexMember].getFine();
                            members[indexMember].bayarDenda(jumlah);
                            System.out.println(members[indexMember].getName() + " berhasil membayar lunas denda");
                            System.out.println("Jumlah kembalian: Rp " + kembalian );
                        }
                    } else {
                        System.out.println(members[indexMember].getName() + " tidak memiliki denda");
                    }
                } else {
                    System.out.println("Anggota dengan ID " + id + " tidak ditemukan");
                }
            
            // Program Detail Anggota
            } else if (command == 7) {
                System.out.println("---------- Detail Anggota ----------");
                System.out.print("ID Anggota: ");
                String id = input.nextLine();


                // Validasi dan menjalankan method detail pada Class Member
                if (foundID(id)) {
                    members[indexMember].detail();
                }
            
            // Program Peringkat Anggota
            } else if (command == 8) {
                System.out.println("---------- Peringkat Anggota ----------");
                Member[] peringkat = new Member[members.length];
                
                // Menyalin Array members ke Array peringkat
                if (members != null) {
                    for (int i = 0; i < members.length; i++) {
                        peringkat[i] = members[i];
                    }
                }

                // Mengurutkan Anggota berdasarkan point
                Member temp;
                for (int i = 0; i < peringkat.length; i++) {  
                    for (int j = i + 1; j < peringkat.length; j++) {  
                        if (peringkat[i].getPoint() < peringkat[j].getPoint()) {  
                            temp = peringkat[i];  
                            peringkat[i] = peringkat[j];  
                            peringkat[j] = temp;  
                        }  
                    }  
                }

                // Mengurutkan Anggota berdasarkan Alphabet
                for (int i = 0; i < peringkat.length; i++) {
                    int smallest = i;
                    for (int j = i + 1; j < peringkat.length; j++) {
                        if(peringkat[i].getPoint() == peringkat[j].getPoint()) {
                            if (peringkat[j].getName().compareTo(peringkat[i].getName()) < 0) {
                                smallest = j;
                            }
                        }
                    }

                    Member temp2 = peringkat[i];
                    peringkat[i] = peringkat[smallest];
                    peringkat[smallest] = temp2;

                }   

                // Mencetak 3 Peringkat teratas
                if (peringkat.length >= 3) {
                    for (int i = 0; i < 3; i++) {
                        System.out.println("—------------- " + (i + 1) + " —-------------");
                        System.out.println(peringkat[i].toString());
                    }
                } else {
                    for (int i = 0; i < peringkat.length; i++) {
                        System.out.println("—------------- " + (i + 1) + " —-------------");
                        System.out.println(peringkat[i].toString());
                    }
                }
                

            // Program Keluar
            } else if (command == 99) {
                System.out.println("Terima kasih telah menggunakan SistakaNG!");
                hasChosenExit = true;
            } else {
                System.out.println("Menu tidak dikenal!");
            }
            System.out.println();
        }

        input.close();
    }
}
