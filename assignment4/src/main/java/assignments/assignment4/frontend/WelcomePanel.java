package assignments.assignment4.frontend;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WelcomePanel extends SistakaPanel {
    public WelcomePanel(HomeGUI homeGUI) {
        super(homeGUI);
        // set frame
        main.stylize(this);
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        // panel
        JPanel labelPanel = new JPanel();
        JPanel buttonPanel = new JPanel(new GridLayout(1,2));
        main.stylize(labelPanel);
        main.stylize(buttonPanel);

        // title
        JLabel welcomeLabel = new JLabel("Welcome to SistakaNG");
        welcomeLabel.setHorizontalAlignment(JLabel.CENTER);
        welcomeLabel.setFont(main.getFontTitle());;
        
        // button
        JButton login = new JButton("Login");
        login.addActionListener(e -> main.setPanel("login"));
        JButton exit = new JButton("Exit");
        exit.addActionListener(e -> System.exit(0));
        
        // add to panel
        labelPanel.add(welcomeLabel);
        buttonPanel.add(login);
        buttonPanel.add(exit);
        gbc.gridx = 1;
        gbc.gridy = 1;
        this.add(labelPanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        this.add(buttonPanel, gbc);

    }


    @Override
    public void refresh() {
        // ignored
    }

}
