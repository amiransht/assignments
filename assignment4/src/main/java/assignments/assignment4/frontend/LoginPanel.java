package assignments.assignment4.frontend;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.pengguna.Pengguna;
import assignments.assignment4.backend.pengguna.Staf;
import assignments.assignment4.frontend.staf.ui.StafHomePanel;

import javax.swing.*;
import java.awt.*;


public class LoginPanel extends SistakaPanel {
    // atribut
    private JTextField entryID;
    private static Pengguna loggedIn;

    public LoginPanel(HomeGUI main) {
        super(main);
        // set frame
        main.stylize(this);
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        // content
        JLabel labelID = new JLabel("Masukkan ID Anda untuk login ke sistem");
        labelID.setHorizontalAlignment(JLabel.CENTER);
        main.stylize(labelID);
        entryID = new JTextField(10);
        
        // button
        JButton buttonLogin = new JButton("Login");
        buttonLogin.addActionListener(e -> login());

        gbc.gridx = 1;
        gbc.gridy = 1;
        this.add(labelID, gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        this.add(entryID, gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        this.add(buttonLogin, gbc);
    }

    @Override
    public void refresh() {
       entryID.setText("");
    }

    public void login() {
        String id = entryID.getText();

        if (id.equals("")) {
            JOptionPane.showMessageDialog(null, "Harap masukkan id Anda pada kotak diatas!");
            refresh();
            return;
        }
        loggedIn = SistakaNG.handleLogin(id);
        if(loggedIn != null) {
            main.setUser(loggedIn);
            if(loggedIn instanceof Staf) {
                main.setPanel("staf");
            } else {
                main.setPanel("anggota");
            }
        } else {
            JOptionPane.showMessageDialog(null, String.format("Pengguna dengan ID %s tidak ditemukan", id));
            refresh();
            return;
        }
    }

    public static Pengguna getLoggedIn() {return loggedIn;}



}
