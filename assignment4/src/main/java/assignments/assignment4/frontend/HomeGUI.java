package assignments.assignment4.frontend;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.pengguna.Pengguna;
import assignments.assignment4.frontend.anggota.ui.*;
import assignments.assignment4.frontend.staf.ui.*;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class HomeGUI {
    private final CardLayout layout = new CardLayout();
    private final JFrame frame;
    private final JPanel mainPanel = new JPanel();
    private final Map<String, SistakaPanel> panelMap = new HashMap<>();
    public static Font fontTitle = new Font("Consolas", Font.BOLD, 30);
    public static Font fontGeneral = new Font("Times New Roman", Font.PLAIN , 18);
    public static Color backColor1 = Color.decode("#F7E2E2");
    public static Color backColor2 = Color.decode("#61A4BC");
    public static Color foreColor = Color.decode("#1A132F");
    private Pengguna user;
    private String namaUser;

    // Silahkan mengganti settingnya sesuai dengan keinginan Anda
    public HomeGUI(JFrame frame) {
        this.frame = frame;
        mainPanel.setLayout(layout);
        initGUI();
        frame.setContentPane(mainPanel);

        if (user != null) this.namaUser = user.getNama();

    }

    public void stylize(Component comp) {
        if (comp instanceof JPanel) {
            comp.setBackground(backColor1);
            comp.setForeground(foreColor);
        } else if (comp instanceof JLabel) {
            comp.setFont(fontGeneral);
        } else if (comp instanceof JFrame) {
            comp.setBackground(backColor2);
        }
    }

    private void initGUI() {
        frame.setSize(400, 200);
        SistakaPanel welcomePanel = new WelcomePanel(this);
        panelMap.put("welcome", welcomePanel);
        mainPanel.add(welcomePanel, "welcome");
        SistakaPanel loginPanel = new LoginPanel(this);
        panelMap.put("login", welcomePanel);
        mainPanel.add(loginPanel, "login");
        SistakaPanel anggotaPanel = new AnggotaHomePanel(this);
        panelMap.put("anggota", anggotaPanel);
        mainPanel.add(anggotaPanel, "anggota");
        SistakaPanel stafPanel = new StafHomePanel(this);
        panelMap.put("staf", stafPanel);
        mainPanel.add(stafPanel, "staf");
        SistakaPanel tambahMahasiswa = new TambahMahasiswaPanel(this);
        panelMap.put("tambahMhs", tambahMahasiswa);
        mainPanel.add(tambahMahasiswa, "tambahMhs");
        SistakaPanel tambahDosen = new TambahDosenPanel(this);
        panelMap.put("tambahDosen", tambahDosen);
        mainPanel.add(tambahDosen, "tambahDosen");
        SistakaPanel tambahKategori = new TambahKategoriPanel(this);
        panelMap.put("tambahKategori", tambahKategori);
        mainPanel.add(tambahKategori, "tambahKategori");
        SistakaPanel tambahBuku = new TambahBukuPanel(this);
        panelMap.put("tambahBuku", tambahBuku);
        mainPanel.add(tambahBuku, "tambahBuku");
        SistakaPanel hapusBuku = new HapusBukuPanel(this);
        panelMap.put("hapusBuku", hapusBuku);
        mainPanel.add(hapusBuku, "hapusBuku");
        SistakaPanel peringkat = new PeringkatPanel(this);
        panelMap.put("peringkat", peringkat);
        mainPanel.add(peringkat, "peringkat");
        SistakaPanel detailAnggota = new DetailAnggotaPanel(this);
        panelMap.put("detailAnggota", detailAnggota);
        mainPanel.add(detailAnggota, "detailAnggota");
        SistakaPanel daftarPeminjam = new DaftarPeminjamPanel(this);
        panelMap.put("daftarPeminjam", daftarPeminjam);
        mainPanel.add(daftarPeminjam, "daftarPeminjam");
        SistakaPanel peminjaman = new PeminjamanPanel(this);
        panelMap.put("peminjaman", peminjaman);
        mainPanel.add(peminjaman, "peminjaman");
        SistakaPanel pengembalian = new PengembalianPanel(this);
        panelMap.put("pengembalian", pengembalian);
        mainPanel.add(pengembalian, "pengembalian");
        SistakaPanel pembayaran = new PembayaranPanel(this);
        panelMap.put("pembayaran", pembayaran);
        mainPanel.add(pembayaran, "pembayaran");
        SistakaPanel detailUser= new DetailUserPanel(this);
        panelMap.put("detailUser", detailUser);
        mainPanel.add(detailUser, "detailUser");
    }

    public Pengguna getUser() {
        return user;
    }


    public void setUser(Pengguna user) {
        SistakaNG.setPenggunaLoggedIn(user);
        this.user = user;
    }

    public JFrame getFrame() {
        return frame;
    }

    public JPanel getPanel(String target) {return panelMap.get(target);}

    public void setPanel(String target){
        panelMap.get(target).refresh();
        layout.show(mainPanel, target);
    }

    public Font getFontGeneral() {
        return fontGeneral;
    }

    public Font getFontTitle() {
        return fontTitle;
    }

    public Color getForecolor() {
        return foreColor;
    }

    public void exit() {
        frame.dispose();
    }

}
