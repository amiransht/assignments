package assignments.assignment4.frontend.anggota.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.buku.Buku;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;


public class PengembalianPanel extends SistakaPanel {
    private JComboBox<String> entryBuku;
    private JTextField entryTanggalKembali;
    public PengembalianPanel(HomeGUI main) {
        super(main);

        // set frame
        setLayout((new GridBagLayout()));
        GridBagConstraints gbc = new GridBagConstraints();
        main.stylize(this);

        // Panel
        JPanel titlePanel = new JPanel();
        JPanel mainPanel = new JPanel(new GridLayout(2,2,10,5));
        JPanel buttonPanel = new JPanel();
        main.stylize(titlePanel);
        main.stylize(mainPanel);
        main.stylize(buttonPanel);

        // Title
        JLabel title = new JLabel("Pengembalian Buku");
        title.setFont(main.getFontTitle());
        title.setHorizontalAlignment(JLabel.CENTER);

        // Content
        JLabel labelBuku = new JLabel("Buku");
        main.stylize(labelBuku);
        entryBuku = new JComboBox<String>();
        JLabel labelTanggalKembali = new JLabel("Tanggal Pengembalian (DD/MM/YYY)");
        main.stylize(labelTanggalKembali);
        entryTanggalKembali = new JTextField(8);


        // Button
        JButton buttonKembalikan = new JButton("Kembalikan");
        buttonKembalikan.addActionListener(e -> kembalikan());
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.addActionListener( e -> main.setPanel("anggota"));

        // add to panel
        titlePanel.add(title);
        mainPanel.add(labelBuku);
        mainPanel.add(entryBuku);
        mainPanel.add(labelTanggalKembali);
        mainPanel.add(entryTanggalKembali);
        buttonPanel.add(buttonKembalikan);
        buttonPanel.add(buttonKembali);

        gbc.gridx = 1;
        gbc.gridy = 1;
        this.add(titlePanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        this.add(mainPanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        this.add(buttonPanel,gbc);
    }

    @Override
    public void refresh() {
        ArrayList<Buku> daftarBuku = SistakaNG.getDaftarBuku();
        if (daftarBuku != null && daftarBuku.size() != 0) {
            String[] buku = new String[daftarBuku.size()];
            for (int i = 0; i < daftarBuku.size(); i++) {
                buku[i] = daftarBuku.get(i).getJudul() + " oleh " + daftarBuku.get(i).getPenulis();
            }
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(buku);
            entryBuku.setModel(model);
        }
        entryBuku.setSelectedIndex(0);
        entryTanggalKembali.setText("");
    }

    public void kembalikan() {
        Buku buku = SistakaNG.getDaftarBuku().get(entryBuku.getSelectedIndex());
        String tanggalKembali = entryTanggalKembali.getText();

        if (tanggalKembali.length() != 10 || tanggalKembali.indexOf("/") != 2 || tanggalKembali.indexOf("/", 3) != 5) {
            JOptionPane.showMessageDialog(null, "Tanggal yang dimasukkan harus dalam format DD/MM/YYYY");
            return;
        } 
        JOptionPane.showMessageDialog(null, SistakaNG.kembalikanBuku(buku, tanggalKembali));
        refresh();
    }
}

