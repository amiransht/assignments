package assignments.assignment4.frontend.anggota.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.pengguna.Anggota;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;

import javax.swing.*;
import java.awt.*;



public class DetailUserPanel extends SistakaPanel {
    private JPanel mainPanel;
    public DetailUserPanel(HomeGUI main) {
        super(main);
        // Set frame
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(new GridBagLayout());
        main.stylize(this);

        // Panel
        JPanel basePanel = new JPanel(new GridBagLayout());
        JPanel titlePanel = new JPanel();
        mainPanel = new JPanel();
        mainPanel.setVisible(false);
        JPanel buttonPanel = new JPanel();
        main.stylize(basePanel);
        main.stylize(titlePanel);
        main.stylize(mainPanel);
        main.stylize(buttonPanel);
    
        // Title
        JLabel title = new JLabel("Lihat Detail Anggota");
        title.setFont(main.getFontTitle());
        title.setHorizontalAlignment(JLabel.CENTER);

        // Button
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.addActionListener( e -> main.setPanel("anggota"));

        // add to Panel
        titlePanel.add(title);
        buttonPanel.add(buttonKembali);

        gbc.gridx = 1;
        gbc.gridy = 1;
        basePanel.add(titlePanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        basePanel.add(mainPanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        basePanel.add(buttonPanel,gbc);
        
        this.add(basePanel);
    }

    @Override
    public void refresh() {
        if (mainPanel != null) mainPanel.removeAll();
        JLabel detailUser;
        Anggota user = (Anggota) main.getUser();
        
        if (user != null) {
            detailUser = new JLabel(user.detail());
            main.stylize(detailUser);
            this.mainPanel.add(detailUser);
            this.mainPanel.setVisible(true);
        } 
        
    }
}
