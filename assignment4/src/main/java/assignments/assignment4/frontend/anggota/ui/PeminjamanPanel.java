package assignments.assignment4.frontend.anggota.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.buku.Buku;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PeminjamanPanel extends SistakaPanel {
    private JComboBox<String> entryBuku;
    private JTextField entryTanggalPinjam;
    public PeminjamanPanel(HomeGUI main) {
        super(main);
        // set frame
        setLayout((new GridBagLayout()));
        GridBagConstraints gbc = new GridBagConstraints();
        main.stylize(this);

        // Panel
        JPanel titlePanel = new JPanel();
        JPanel mainPanel = new JPanel(new GridLayout(2,2,10,5));
        JPanel buttonPanel = new JPanel();
        main.stylize(titlePanel);
        main.stylize(mainPanel);
        main.stylize(buttonPanel);

        // Title
        JLabel title = new JLabel("Pinjam Buku");
        title.setFont(main.getFontTitle());
        title.setHorizontalAlignment(JLabel.CENTER);

        // Content
        JLabel labelBuku = new JLabel("Buku");
        main.stylize(labelBuku);
        entryBuku = new JComboBox<String>();
        JLabel labelTanggalPinjam = new JLabel("Tanggal Peminjaman (DD/MM/YYY)");
        main.stylize(labelTanggalPinjam);
        entryTanggalPinjam = new JTextField(8);

        // Button
        JButton buttonPinjam = new JButton("Pinjam");
        buttonPinjam.addActionListener(e -> pinjam());
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.addActionListener( e -> main.setPanel("anggota"));


        // add to panel
        titlePanel.add(title);
        mainPanel.add(labelBuku);
        mainPanel.add(entryBuku);
        mainPanel.add(labelTanggalPinjam);
        mainPanel.add(entryTanggalPinjam);
        buttonPanel.add(buttonPinjam);
        buttonPanel.add(buttonKembali);

        gbc.gridx = 1;
        gbc.gridy = 1;
        this.add(titlePanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        this.add(mainPanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        this.add(buttonPanel,gbc);

    }

    @Override
    public void refresh() {
        ArrayList<Buku> daftarBuku = SistakaNG.getDaftarBuku();
        if (daftarBuku != null && daftarBuku.size() != 0) {
            String[] buku = new String[daftarBuku.size()];
            for (int i = 0; i < daftarBuku.size(); i++) {
                buku[i] = daftarBuku.get(i).getJudul() + " oleh " + daftarBuku.get(i).getPenulis();
            }
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(buku);
            entryBuku.setModel(model);
        }
        entryBuku.setSelectedItem(null);;
        entryTanggalPinjam.setText("");
    }

    public void pinjam() {
        Buku buku = SistakaNG.getDaftarBuku().get(entryBuku.getSelectedIndex());
        String tanggalPinjam = entryTanggalPinjam.getText();

        if (tanggalPinjam.length() != 10 || tanggalPinjam.indexOf("/") != 2 || tanggalPinjam.indexOf("/", 3) != 5) {
            JOptionPane.showMessageDialog(null, "Tanggal yang dimasukkan harus dalam format DD/MM/YYYY");
            return;
        } 
        JOptionPane.showMessageDialog(null, SistakaNG.pinjamBuku(buku, entryTanggalPinjam.getText()));
        refresh();

    }
}
