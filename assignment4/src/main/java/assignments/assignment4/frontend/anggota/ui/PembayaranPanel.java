package assignments.assignment4.frontend.anggota.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;


public class PembayaranPanel extends SistakaPanel {
    private JTextField entryJumlah;
    public PembayaranPanel(HomeGUI main) {
        super(main);

        // Set Frame
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(new GridBagLayout());
        main.stylize(this);

        // Panel
        JPanel titlePanel = new JPanel();
        JPanel mainPanel = new JPanel(new GridLayout(1,2,5,5));
        JPanel buttonPanel = new JPanel();
        main.stylize(titlePanel);
        main.stylize(mainPanel);
        main.stylize(buttonPanel);

        // Title
        JLabel title = new JLabel("Bayar Denda");
        title.setFont(main.getFontTitle());
        title.setHorizontalAlignment(JLabel.CENTER);

        // Content
        JLabel labelJumlah = new JLabel("Jumlah Denda");
        main.stylize(labelJumlah);
        entryJumlah = new JTextField(8);

        // Button
        JButton buttonBayar = new JButton("Bayar");
        buttonBayar.addActionListener(e -> bayar());
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.addActionListener( e -> main.setPanel("anggota"));


        // add to panel
        titlePanel.add(title);
        mainPanel.add(labelJumlah);
        mainPanel.add(entryJumlah);
        buttonPanel.add(buttonBayar);
        buttonPanel.add(buttonKembali);

        gbc.gridx = 1;
        gbc.gridy = 1;
        this.add(titlePanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        this.add(mainPanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        this.add(buttonPanel, gbc);

    }

    @Override
    public void refresh() {
        entryJumlah.setText("");

    }

    public void bayar() {
        if (!isNumeric(entryJumlah.getText())) {
            JOptionPane.showMessageDialog(null, "Jumlah Bayar harus berupa angka!");
            refresh();
        } else {
            JOptionPane.showMessageDialog(null, SistakaNG.bayarDenda(Long.parseLong(entryJumlah.getText())));
            refresh();
        }
    }
}
