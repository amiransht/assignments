package assignments.assignment4.frontend.anggota.ui;


import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;

import javax.swing.*;
import java.awt.*;



public class AnggotaHomePanel extends SistakaPanel {
    private JPanel welcomePanel;
    private String nama;
    private JLabel welcomeLabel;
    public AnggotaHomePanel(HomeGUI main) {
        super(main);
        // Set Frame
        setLayout(new GridLayout(10,1));
        setSize(600, 200);
        main.stylize(this);

        // Panel
        welcomePanel = new JPanel();
        welcomePanel.setVisible(false);
        main.stylize(welcomePanel);

        // Button
        JButton buttonPeminjaman = new JButton("Peminjaman");
        buttonPeminjaman.addActionListener(e -> main.setPanel("peminjaman"));
        JButton buttonPengembalian = new JButton("Pengembalian");
        buttonPengembalian.addActionListener(e -> main.setPanel("pengembalian"));
        JButton buttonBayarDenda = new JButton("Pembayaran Denda");
        buttonBayarDenda.addActionListener(e -> main.setPanel("pembayaran"));
        JButton buttonDetailAgt = new JButton("Detail Anggota");
        buttonDetailAgt.addActionListener(e -> main.setPanel("detailUser"));
        JButton buttonLogout = new JButton("Logout");
        buttonLogout.addActionListener(e -> main.setPanel("login"));

        // add widget to Panel
        this.add(welcomePanel);
        this.add(buttonPeminjaman);
        this.add(buttonPengembalian);
        this.add(buttonBayarDenda);
        this.add(buttonDetailAgt);
        this.add(buttonLogout);
    }

    @Override
    public void refresh() {
        if (welcomePanel != null) welcomePanel.removeAll();
        if (main.getUser() != null) {
            nama = main.getUser().getNama();
            welcomeLabel = new JLabel("Selamat datang kembali " + nama + "!");
            welcomeLabel.setHorizontalAlignment(JLabel.CENTER);


            welcomePanel.add(welcomeLabel);
            welcomePanel.setVisible(true);
        }

    }

}
