package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.buku.Buku;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;


public class DaftarPeminjamPanel extends SistakaPanel {
    private JComboBox<String> entryBuku;
    private JPanel mainPanel, detailPanel, buttonPanel;

    public DaftarPeminjamPanel(HomeGUI main) {
        super(main);
        // Set frame
        setLayout((new GridBagLayout()));
        GridBagConstraints gbc = new GridBagConstraints();
        main.stylize(this);

        // Panel
        JPanel basePanel = new JPanel(new GridBagLayout());
        JPanel titlePanel = new JPanel();
        detailPanel = new JPanel(new GridBagLayout());
        detailPanel.setVisible(false);
        mainPanel = new JPanel(new GridLayout(3,1,10,5));
        buttonPanel = new JPanel();
        main.stylize(basePanel);
        main.stylize(titlePanel);
        main.stylize(mainPanel);
        main.stylize(buttonPanel);
        main.stylize(detailPanel);

        // Title
        JLabel title = new JLabel("Lihat Daftar Peminjam");
        title.setHorizontalAlignment(JLabel.CENTER);
        title.setFont(main.getFontTitle());

        // Content
        JLabel labelBuku = new JLabel("Pilih Buku");
        main.stylize(labelBuku);
        labelBuku.setHorizontalAlignment(JLabel.CENTER);
        entryBuku = new JComboBox<String>();

        // Button
        JButton buttonLihat = new JButton("Lihat");
        buttonLihat.addActionListener(e -> lihat());
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.addActionListener( e -> main.setPanel("staf"));

        // add to panel
        titlePanel.add(title);
        mainPanel.add(labelBuku);
        mainPanel.add(entryBuku);
        buttonPanel.add(buttonLihat);
        buttonPanel.add(buttonKembali);

        gbc.gridx = 1;
        gbc.gridy = 1;
        basePanel.add(titlePanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        basePanel.add(mainPanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        basePanel.add(detailPanel,gbc);
        gbc.gridx = 1;
        gbc.gridy = 4;
        basePanel.add(buttonPanel,gbc);

        this.add(basePanel);
    }

    @Override
    public void refresh() {
        
        ArrayList<Buku> daftarBuku = SistakaNG.getDaftarBuku();
        if (daftarBuku != null && daftarBuku.size() != 0) {
            String[] buku = new String[daftarBuku.size()];
            for (int i = 0; i < daftarBuku.size(); i++) {
                buku[i] = daftarBuku.get(i).getJudul() + " oleh " + daftarBuku.get(i).getPenulis();
            }
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(buku);
            entryBuku.setModel(model);
        }

        detailPanel.removeAll();
        entryBuku.setSelectedItem(null);
    }

    public void setMainPanel(boolean saklar) {
        this.mainPanel.setVisible(saklar);
        this.buttonPanel.setVisible(saklar);
    }

    public void setDetailPanel(boolean saklar) {
        this.detailPanel.setVisible(saklar);

    }

    public void lihat() {
        if (entryBuku.getSelectedItem() != null) {
            int indexBuku = entryBuku.getSelectedIndex();
            Buku buku = SistakaNG.getDaftarBuku().get(indexBuku);

            JLabel detailPeminjam = new JLabel(SistakaNG.daftarPeminjam(buku));
            main.stylize(detailPeminjam);
            JButton buttonKembali = new JButton("Kembali");
            buttonKembali.addActionListener( e -> kembali());

            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 1;
            gbc.gridy = 1;
            detailPanel.add(detailPeminjam, gbc);
            gbc.gridx = 1;
            gbc.gridy = 2;
            detailPanel.add(buttonKembali, gbc);

            setMainPanel(false);
            setDetailPanel(true);


        } else {
            JOptionPane.showMessageDialog(null, "Silahkan memilih buku!");
            refresh();
        }
    }

    public void kembali() {
        refresh();
        setMainPanel(true);
        setDetailPanel(false);
    }
}
