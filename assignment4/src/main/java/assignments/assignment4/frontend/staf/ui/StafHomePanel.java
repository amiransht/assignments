package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.LoginPanel;
import assignments.assignment4.frontend.SistakaPanel;


import javax.swing.*;
import java.awt.*;


public class StafHomePanel extends SistakaPanel {
    private JPanel welcomePanel;
    private String nama;
    private JLabel welcomeLabel;
    public StafHomePanel(HomeGUI main) {
        super(main);
        // set frame
        setLayout(new GridLayout(10,1));
        main.getFrame().setSize(400, 600);
        main.stylize(this);

        // Panel
        welcomePanel = new JPanel();
        welcomePanel.setVisible(false);
        main.stylize(welcomePanel);
       
       
        // Content
        JButton buttonTambahMhs = new JButton("Tambah Mahasiswa");
        buttonTambahMhs.addActionListener(e -> main.setPanel("tambahMhs"));
        JButton buttonTambahDosen = new JButton("Tambah Dosen");
        buttonTambahDosen.addActionListener(e -> main.setPanel("tambahDosen"));
        JButton buttonTambahKtg = new JButton("Tambah Kategori");
        buttonTambahKtg.addActionListener(e -> main.setPanel("tambahKategori"));
        JButton buttonTambahBuku = new JButton("Tambah Buku");
        buttonTambahBuku.addActionListener(e -> main.setPanel("tambahBuku"));
        JButton buttonHapusBuku = new JButton("Hapus Buku");
        buttonHapusBuku.addActionListener(e -> main.setPanel("hapusBuku"));
        JButton buttonPeringkat = new JButton("3 Peringkat Pertama");
        buttonPeringkat.addActionListener(e -> main.setPanel("peringkat"));
        JButton buttonDetailAgt = new JButton("Detail Anggota");
        buttonDetailAgt.addActionListener(e -> main.setPanel("detailAnggota"));
        JButton buttonDaftarPeminjam = new JButton("Daftar Peminjaman Buku");
        buttonDaftarPeminjam.addActionListener(e -> main.setPanel("daftarPeminjam"));
        JButton buttonLogout = new JButton("Logout");
        buttonLogout.addActionListener(e -> main.setPanel("login"));

        // add to panel
        this.add(welcomePanel);
        this.add(buttonTambahMhs);
        this.add(buttonTambahDosen);
        this.add(buttonTambahKtg);
        this.add(buttonTambahBuku);
        this.add(buttonHapusBuku);
        this.add(buttonPeringkat);
        this.add(buttonDetailAgt);
        this.add(buttonDaftarPeminjam);
        this.add(buttonLogout);

    }

    @Override
    public void refresh() {
        if (welcomePanel != null) welcomePanel.removeAll();
        if (main.getUser() != null) {
            nama = main.getUser().getNama();
            welcomeLabel = new JLabel("Selamat datang kembali " + nama + "!");
            welcomeLabel.setHorizontalAlignment(JLabel.CENTER);
            main.stylize(welcomeLabel);


            welcomePanel.add(welcomeLabel);
            welcomePanel.setVisible(true);
        }

    }



}
