package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.pengguna.Anggota;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;


public class PeringkatPanel extends SistakaPanel {
    private JPanel mainPanel;
    public PeringkatPanel(HomeGUI main) {
        super(main);
        // set frame
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(new GridBagLayout());
        main.stylize(this);

        // Panel
        JPanel basePanel = new JPanel(new GridBagLayout());
        JPanel titlePanel = new JPanel();
        mainPanel = new JPanel();
        mainPanel.setVisible(false);
        JPanel buttonPanel = new JPanel();
        main.stylize(basePanel);
        main.stylize(titlePanel);
        main.stylize(mainPanel);
        main.stylize(buttonPanel);

        // Content
        JLabel title = new JLabel("Peringkat");
        title.setHorizontalAlignment(JLabel.CENTER);
        title.setFont(main.getFontTitle());

        // button
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.addActionListener( e -> main.setPanel("staf"));

        // add to panel
        titlePanel.add(title);
        buttonPanel.add(buttonKembali);

        gbc.gridx = 1;
        gbc.gridy = 1;
        basePanel.add(titlePanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        basePanel.add(mainPanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        basePanel.add(buttonPanel,gbc);

        this.add(basePanel);

    }

    @Override
    public void refresh() {
        if (mainPanel != null) mainPanel.removeAll();
        JLabel peringkat;

        ArrayList<Anggota> daftarAnggota = SistakaNG.getDaftarAnggota();
        if (daftarAnggota!= null && daftarAnggota.size() != 0) {
            peringkat = new JLabel(SistakaNG.handleRankingAnggota());
        } else {
            peringkat = new JLabel("Belum ada anggota yang terdaftar pada sistem");
        }
        
        main.stylize(peringkat);
        this.mainPanel.add(peringkat);
        this.mainPanel.setVisible(true);
    }
}
