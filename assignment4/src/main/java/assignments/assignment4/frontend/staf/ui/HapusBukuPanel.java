package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.buku.Buku;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;


public class HapusBukuPanel extends SistakaPanel {
    private JComboBox<String> entryBuku;

    public HapusBukuPanel(HomeGUI main) {
        super(main);
        // set frame
        setLayout((new GridBagLayout()));
        GridBagConstraints gbc = new GridBagConstraints();
        main.stylize(this);

        // Panel
        JPanel titlePanel = new JPanel();
        JPanel mainPanel = new JPanel(new GridLayout(3,1,10,5));
        JPanel buttonPanel = new JPanel();
        main.stylize(titlePanel);
        main.stylize(mainPanel);
        main.stylize(buttonPanel);

        // Content
        JLabel title = new JLabel("Hapus Buku");
        title.setHorizontalAlignment(JLabel.CENTER);
        title.setFont(main.getFontTitle());

        JLabel labelBuku = new JLabel("Buku");
        main.stylize(labelBuku);
        labelBuku.setHorizontalAlignment(JLabel.CENTER);
        entryBuku = new JComboBox<String>();

        // Button
        JButton buttonHapus = new JButton("Hapus");
        buttonHapus.addActionListener(e -> hapus());
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.addActionListener( e -> main.setPanel("staf"));

        // add to panel
        titlePanel.add(title);
        mainPanel.add(labelBuku);
        mainPanel.add(entryBuku);
        buttonPanel.add(buttonHapus);
        buttonPanel.add(buttonKembali);

        gbc.gridx = 1;
        gbc.gridy = 1;
        this.add(titlePanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        this.add(mainPanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        this.add(buttonPanel,gbc);

    }

    @Override
    public void refresh() {
        entryBuku.setSelectedItem(null);
        ArrayList<Buku> daftarBuku = SistakaNG.getDaftarBuku();
        if (daftarBuku != null && daftarBuku.size() != 0) {
            String[] buku = new String[daftarBuku.size()];
            for (int i = 0; i < daftarBuku.size(); i++) {
                buku[i] = daftarBuku.get(i).getJudul() + " oleh " + daftarBuku.get(i).getPenulis();
            }
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(buku);
            entryBuku.setModel(model);
        }

    }

    public void hapus() {
        if (entryBuku.getSelectedItem() != null) {
            int indexBuku = entryBuku.getSelectedIndex();
            JOptionPane.showMessageDialog(null, SistakaNG.deleteBuku(SistakaNG.getDaftarBuku().get(indexBuku)));
            refresh();
        } else {
            JOptionPane.showMessageDialog(null, "Silahkan memilih buku!");
            refresh();
        }

    }

}
