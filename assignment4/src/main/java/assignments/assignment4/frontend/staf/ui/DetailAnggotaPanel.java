package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.pengguna.Anggota;
import assignments.assignment4.backend.pengguna.Mahasiswa;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;

import javax.swing.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;


public class DetailAnggotaPanel extends SistakaPanel {
    private JPanel basePanel, mainPanel, titlePanel, detailPanel, buttonPanel;
    private JComboBox<String> entryIDAnggota;
    JButton buttonLihat, buttonKembali;

    public DetailAnggotaPanel(HomeGUI main) {
        super(main);
        // Set Frame
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(new GridBagLayout());
        main.stylize(this);

        //Panel
        basePanel = new JPanel(new GridBagLayout());
        titlePanel = new JPanel();
        mainPanel = new JPanel();
        buttonPanel = new JPanel();
        detailPanel = new JPanel(new GridBagLayout());
        detailPanel.setVisible(false);
        main.stylize(basePanel);
        main.stylize(titlePanel);
        main.stylize(mainPanel);
        main.stylize(buttonPanel);
        main.stylize(detailPanel);

        // Title
        JLabel title = new JLabel("Lihat Detail Anggota");
        title.setHorizontalAlignment(JLabel.CENTER);
        title.setFont(main.getFontTitle());

        // content
        JLabel labelPilihID = new JLabel("Pilih ID Anggota");
        main.stylize(labelPilihID);
        labelPilihID.setHorizontalAlignment(JLabel.CENTER);

        entryIDAnggota = new JComboBox<String>();

        // Button
        buttonLihat = new JButton("Lihat");
        buttonLihat.addActionListener(e -> lihat());
        buttonKembali = new JButton("Kembali");
        buttonKembali.addActionListener( e -> main.setPanel("staf"));
        
        // add to panel
        titlePanel.add(title);
        mainPanel.add(labelPilihID);
        mainPanel.add(entryIDAnggota);
        buttonPanel.add(buttonLihat);
        buttonPanel.add(buttonKembali);
        

        gbc.gridx = 1;
        gbc.gridy = 1;
        basePanel.add(titlePanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        basePanel.add(mainPanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        basePanel.add(detailPanel,gbc);
        gbc.gridx = 1;
        gbc.gridy = 4;
        basePanel.add(buttonPanel,gbc);

        this.add(basePanel);

    }

    @Override
    public void refresh() {

        ArrayList<Anggota> daftarAnggota = SistakaNG.getDaftarAnggota();
        if (daftarAnggota != null && daftarAnggota.size() != 0) {
            String[] idAnggota = new String[daftarAnggota.size()];
            for (int i = 0; i < daftarAnggota.size(); i++) {
                idAnggota[i] = daftarAnggota.get(i).getId();
            }
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(idAnggota);
            entryIDAnggota.setModel(model);
        }

        detailPanel.removeAll();
        entryIDAnggota.setSelectedItem(null);;
    }

    // setting visibility panel
    public void setMainPanel(boolean saklar) {
        this.mainPanel.setVisible(saklar);
        this.buttonPanel.setVisible(saklar);
    }

    public void setDetailPanel(boolean saklar) {
        this.detailPanel.setVisible(saklar);

    }

    public void lihat() {
        if(entryIDAnggota.getSelectedItem() == null){
            // error tidak diisi filed
            JOptionPane.showMessageDialog(null,"Silahkan memilih ID Anggota!");
            return;
        }

        // membuat panel detail
        String id = (String) entryIDAnggota.getSelectedItem();
        Anggota anggota = SistakaNG.findAnggota(id);

        JLabel detailAnggota = new JLabel(anggota.detail());
        main.stylize(detailAnggota);
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.addActionListener( e -> kembali());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        detailPanel.add(detailAnggota, gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        detailPanel.add(buttonKembali, gbc);

        setMainPanel(false);
        setDetailPanel(true);
       
    }

    public void kembali() {
        refresh();
        setMainPanel(true);
        setDetailPanel(false);
    }
}
