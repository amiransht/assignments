package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.pengguna.IdGenerator;
import assignments.assignment4.backend.pengguna.Mahasiswa;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class TambahMahasiswaPanel extends SistakaPanel {
    private JTextField entryNama, entryTglLahir, entryAngkatan;
    private JComboBox entryProdi;
    private String[] namaProdi = {"SIK", "SSI", "MIK", "MTI", "DIK"};
    public TambahMahasiswaPanel(HomeGUI main) {
        super(main);
        // set frame
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(new GridBagLayout());
        main.stylize(this);

        // panel
        JPanel titlePanel = new JPanel();
        JPanel mainPanel = new JPanel(new GridLayout(4,2,5,5));
        JPanel buttonPanel = new JPanel();
        main.stylize(titlePanel);
        main.stylize(mainPanel);
        main.stylize(buttonPanel);

        // title
        JLabel title = new JLabel("Tambah Mahasiswa");
        title.setHorizontalAlignment(JLabel.CENTER);
        title.setFont(main.getFontTitle());

        // content
        JLabel labelNama = new JLabel("Nama");
        main.stylize(labelNama);
        entryNama = new JTextField(8);
        JLabel labelTanggalLahir = new JLabel("Tanggal Lahir (DD/MM/YYYY)");
        main.stylize(labelTanggalLahir);
        entryTglLahir = new JTextField(8);
        JLabel labelProdi = new JLabel("Program Studi");
        main.stylize(labelProdi);
        entryProdi = new JComboBox<>(namaProdi);
        JLabel labelAngkatan = new JLabel("Angkatan");
        main.stylize(labelAngkatan);
        entryAngkatan = new JTextField(8);

        // button
        JButton buttonTambah = new JButton("Tambah");
        buttonTambah.addActionListener(e -> tambah((String) entryProdi.getSelectedItem(), entryAngkatan.getText(), entryTglLahir.getText()));
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.addActionListener( e -> main.setPanel("staf"));

        // add to panel
        titlePanel.add(title);
        mainPanel.add(labelNama);
        mainPanel.add(entryNama);
        mainPanel.add(labelTanggalLahir);
        mainPanel.add(entryTglLahir);
        mainPanel.add(labelProdi);
        mainPanel.add(entryProdi);
        mainPanel.add(labelAngkatan);
        mainPanel.add(entryAngkatan);
        buttonPanel.add(buttonTambah);
        buttonPanel.add(buttonKembali);

        gbc.gridx = 1;
        gbc.gridy = 1;

        this.add(titlePanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 4;
        this.add(mainPanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 5;
        this.add(buttonPanel,gbc);

    }

    @Override
    public void refresh() {
        entryNama.setText("");
        entryProdi.setSelectedItem(null);
        entryTglLahir.setText("");
        entryAngkatan.setText("");

    }

    public void tambah(String prodi, String angkatan, String tanggalLahir) {
        Mahasiswa mahasiswa = SistakaNG.addMahasiswa(entryNama.getText(), entryTglLahir.getText(), (String) entryProdi.getSelectedItem(), entryAngkatan.getText());

        if(mahasiswa != null) {
            String id = mahasiswa.getId();
            JOptionPane.showMessageDialog(null, "Berhasil menambahkan mahasiswa dengan ID " + id + "!");
            refresh();
        } else {
            JOptionPane.showMessageDialog(null, "Tidak dapat menambahkan mahasiswa silahkan periksa kembali input anda!");
            refresh();
        }
    }
}
