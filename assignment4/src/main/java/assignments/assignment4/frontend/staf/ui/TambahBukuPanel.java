package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.buku.Buku;
import assignments.assignment4.backend.buku.Kategori;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class TambahBukuPanel extends SistakaPanel {
    private JTextField entryJudul, entryPenulis, entryPenerbit, entryStok;
    private JComboBox<String> entryKategori;
    private String[] namaKategori;

    public TambahBukuPanel(HomeGUI main) {
        super(main);
        // set frame
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        main.stylize(this);

        // Panel
        JPanel titlePanel = new JPanel();
        JPanel mainPanel = new JPanel(new GridLayout(5,2,5,5));
        JPanel buttonPanel = new JPanel();
        main.stylize(titlePanel);
        main.stylize(mainPanel);
        main.stylize(buttonPanel);

        // Title
        JLabel title = new JLabel("Tambah Buku");
        title.setHorizontalAlignment(JLabel.CENTER);
        title.setFont(main.getFontTitle());

        // Content
        JLabel labelJudul = new JLabel("Judul");
        main.stylize(labelJudul);
        entryJudul = new JTextField(8);
        JLabel labelPenulis = new JLabel("Penulis");
        main.stylize(labelPenulis);
        entryPenulis = new JTextField(8);
        JLabel labelPenerbit = new JLabel("Penerbit");
        main.stylize(labelPenerbit);
        entryPenerbit = new JTextField(8);
        JLabel labelKategori = new JLabel("Kategori");
        main.stylize(labelKategori);
        entryKategori = new JComboBox<String>();
        JLabel labelStok = new JLabel("Stok");
        main.stylize(labelStok);
        entryStok = new JTextField(8);

        // Button
        JButton buttonTambah = new JButton("Tambah");
        buttonTambah.addActionListener(e -> tambah());
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.addActionListener( e -> main.setPanel("staf"));

        // add to panel
        titlePanel.add(title);
        mainPanel.add(labelJudul);
        mainPanel.add(entryJudul);
        mainPanel.add(labelPenulis);
        mainPanel.add(entryPenulis);
        mainPanel.add(labelPenerbit);
        mainPanel.add(entryPenerbit);
        mainPanel.add(labelKategori);
        mainPanel.add(entryKategori);
        mainPanel.add(labelStok);
        mainPanel.add(entryStok);
        buttonPanel.add(buttonTambah);
        buttonPanel.add(buttonKembali);

        gbc.gridx = 1;
        gbc.gridy = 1;
        this.add(titlePanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 4;
        this.add(mainPanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 5;
        this.add(buttonPanel, gbc);
    }


    @Override
    public void refresh() {
    
        if (SistakaNG.getDaftarKategori() != null && SistakaNG.getDaftarKategori().size() != 0) {
            namaKategori = new String[SistakaNG.getDaftarKategori().size()];
            for (int i = 0; i < SistakaNG.getDaftarKategori().size(); i++) {
                namaKategori[i] = SistakaNG.getDaftarKategori().get(i).getNama();
            }
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(namaKategori);
            entryKategori.setModel(model);
        }
        entryJudul.setText("");
        entryPenulis.setText("");
        entryPenerbit.setText("");
        entryKategori.setSelectedItem(null);
        entryStok.setText("");
    }


    public void tambah() {
        if (Integer.parseInt(entryStok.getText()) == 0) {
            JOptionPane.showMessageDialog(null, "Stok harus lebih dari 0!");
            refresh();
        }
        Buku findBuku = SistakaNG.findBuku(entryJudul.getText(), entryPenulis.getText());
        if (findBuku != null) {
            JOptionPane.showMessageDialog(null, "Buku " + findBuku.getJudul() + " oleh " +  findBuku.getPenulis() + " sudah pernah ditambahkan");
            refresh();
        }

        Buku buku = SistakaNG.addBuku(entryJudul.getText(),entryPenulis.getText(),
                    entryPenerbit.getText(),entryKategori.getSelectedItem().toString(),
                    Integer.parseInt(entryStok.getText()));

        if(buku != null) {
            JOptionPane.showMessageDialog(null, "Buku " + entryJudul.getText() + " oleh " + entryPenulis.getText() + " berhasil ditambahkan");
            refresh();
        }

    }
}
