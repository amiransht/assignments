package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.buku.Kategori;
import assignments.assignment4.backend.pengguna.Dosen;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;


public class TambahKategoriPanel extends SistakaPanel {
    private JTextField entryNama, entryPoin;

    public TambahKategoriPanel(HomeGUI main) {
        super(main);
        // set frame
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        main.stylize(this);

        // panel
        JPanel titlePanel = new JPanel();
        JPanel mainPanel = new JPanel(new GridLayout(2,2,5,5));
        JPanel buttonPanel = new JPanel();
        main.stylize(titlePanel);
        main.stylize(mainPanel);
        main.stylize(buttonPanel);

        // title
        JLabel title = new JLabel("Tambah Kategori");
        title.setHorizontalAlignment(JLabel.CENTER);
        title.setFont(main.getFontTitle());

        // content
        JLabel labelNama = new JLabel("Nama");
        main.stylize(labelNama);
        entryNama = new JTextField(8);
        JLabel labelPoin = new JLabel("Poin");
        main.stylize(labelPoin);
        entryPoin = new JTextField(8);

        // button
        JButton buttonTambah = new JButton("Tambah");
        buttonTambah.addActionListener(e -> tambah());
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.addActionListener( e -> main.setPanel("staf"));


        // add to panel
        titlePanel.add(title);
        mainPanel.add(labelNama);
        mainPanel.add(entryNama);
        mainPanel.add(labelPoin);
        mainPanel.add(entryPoin);
        buttonPanel.add(buttonTambah);
        buttonPanel.add(buttonKembali);

        gbc.gridx = 1;
        gbc.gridy = 1;

        this.add(titlePanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 4;
        this.add(mainPanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 5;
        this.add(buttonPanel, gbc);
    }

    @Override
    public void refresh() {
        entryNama.setText("");
        entryPoin.setText("");
    }

    public void tambah() {
        Kategori kategori = SistakaNG.addKategori(entryNama.getText(), Integer.parseInt(entryPoin.getText()));
        if (kategori != null) {
            JOptionPane.showMessageDialog(null, "Kategori " + entryNama.getText() +
                                                                        " dengan poin " + Integer.parseInt(entryPoin.getText()) +
                                                                        " berhasil ditambahkan!" );
            refresh();
        } else {
            kategori = SistakaNG.findKategori(entryNama.getText());
            JOptionPane.showMessageDialog(null, "Kategori " + kategori.getNama() + " sudah pernah ditambahkan");
            refresh();
        }
    }
}
