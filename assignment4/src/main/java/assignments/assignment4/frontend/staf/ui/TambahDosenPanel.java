package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.pengguna.Dosen;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;


public class TambahDosenPanel extends SistakaPanel {
    private JTextField entryNama;
    public TambahDosenPanel(HomeGUI main) {
        super(main);
        // set frame
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(new GridBagLayout());
        main.stylize(this);

        // panel
        JPanel titlePanel = new JPanel();
        JPanel mainPanel = new JPanel(new GridLayout(1,2,5,5));
        JPanel buttonPanel = new JPanel();
        main.stylize(titlePanel);
        main.stylize(mainPanel);
        main.stylize(buttonPanel);

        // title
        JLabel title = new JLabel("Tambah Dosen");
        title.setHorizontalAlignment(JLabel.CENTER);
        title.setFont(main.getFontTitle());

        // content
        JLabel labelNama = new JLabel("Nama");
        main.stylize(labelNama);
        entryNama = new JTextField(8);

        // button
        JButton buttonTambah = new JButton("Tambah");
        buttonTambah.addActionListener(e -> tambah());
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.addActionListener( e -> main.setPanel("staf"));

        // add to panel
        titlePanel.add(title);
        mainPanel.add(labelNama);
        mainPanel.add(entryNama);
        buttonPanel.add(buttonTambah);
        buttonPanel.add(buttonKembali);

        gbc.gridx = 1;
        gbc.gridy = 1;
        this.add(titlePanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        this.add(mainPanel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        this.add(buttonPanel, gbc);

    }

    @Override
    public void refresh() {
        entryNama.setText("");
    }

    public void tambah() {
        if (entryNama.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Tidak dapat menambahkan dosen silahkan periksa kembali input anda!");
            return;
        }

        Dosen dosen = SistakaNG.addDosen(entryNama.getText());
        if (dosen != null) {
            String id = dosen.getId();
            JOptionPane.showMessageDialog(null, "Berhasil menambahkan dosen dengan ID " + id + "!");
            refresh();
        } 
    }
}
