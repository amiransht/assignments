package assignments.assignment3;

import assignments.assignment3.buku.Buku;
import assignments.assignment3.buku.Kategori;
import assignments.assignment3.buku.Peminjaman;
import assignments.assignment3.pengguna.*;

import java.util.Arrays;
import java.util.Scanner;

public class SistakaNG {
    private static Scanner input = new Scanner(System.in);
    // Attribute kelas SistakaNG
    public static Pengguna penggunaLoggedIn;
    public static Staf[] daftarStaf;
    public static Anggota[] daftarAnggota;
    public static Buku[] daftarBuku;
    public static Kategori[] daftarKategori;
    private static int indexMember = 0;
    private static int indexBook = 0;
    private static int indexBookLoan = 0;
    private static int indexCategory = 0;

    // Program utama
    public static void main(String[] args) {
        System.out.println("Start - Register Staf...");
        registerStaf();
        System.out.println("Done - Register Staf...\n");
        menu();
        input.close();
    }

    // Method ini digunakan untuk mendaftarkan staf-staf ketika program dijalankan
    private static void registerStaf() {
        String[] listNama = new String[]{"Dek Depe", "Dek DePram", "Dek Sofita", "Winter", "Boo"};
        daftarStaf = new Staf[0];

        for (String s : listNama) {
            Staf[] newStaffs = new Staf[daftarStaf.length + 1];

            if (daftarStaf.length != 0) System.arraycopy(daftarStaf, 0, newStaffs, 0, daftarStaf.length);

            Staf staf = new Staf(s);
            newStaffs[newStaffs.length - 1] = staf;
            daftarStaf = newStaffs.clone();


            System.out.println("Berhasil menambahkan staf dengan data:");
            System.out.println(staf);
        }
    }

    // Method ini digunakan untuk mencetak menu utama dari SistakaNG
    public static void menu() {
        boolean hasChosenExit = false;
        System.out.println("Selamat Datang di Sistem Perpustakaan SistakaNG!");
        while (!hasChosenExit) {
            int command;
            System.out.println("================ Menu Utama ================\n");
            System.out.println("1. Login");
            System.out.println("2. Keluar");
            System.out.print("Masukkan pilihan menu: ");
            command = Integer.parseInt(input.nextLine());
            System.out.println();
            if (command == 1) {
                menuLogin();
            } else if (command == 2) {
                System.out.println("Terima kasih telah menggunakan SistakaNG. Sampai jumpa di lain kesempatan!");
                hasChosenExit = true;
            } else {
                System.out.println("Menu tidak dikenal!");
            }
            System.out.println();
        }
    }

    // Method ini digunakan untuk mencetak menu login
    public static void menuLogin() {
        boolean isLoginSuccess = false;
        while (!isLoginSuccess) {
            System.out.println("Masukkan ID Anda untuk login ke sistem");
            System.out.print("ID: ");
            String id = input.nextLine();

            if (id.startsWith("STAF") && daftarStaf != null) {
                for (Staf staf : daftarStaf) {
                    if (staf.getID().equals(id)) {
                        isLoginSuccess = true;
                        penggunaLoggedIn = staf;
                        System.out.println("Halo, " + staf.getNama()
                                + "! Selamat datang di SistakaNG");
                    }
                }
            } else if (daftarAnggota != null) {
                for (Anggota anggota : daftarAnggota) {
                    if (anggota.getID().equals(id)) {
                        isLoginSuccess = true;
                        penggunaLoggedIn = anggota;
                        System.out.println("Halo, " + anggota.getNama() + "! Selamat datang di SistakaNG");
                    }
                }
            }
          

            if (!isLoginSuccess) System.out.println("Pengguna dengan ID "+ id + " tidak ditemukan");
        }

        showMenu();
    }

    // Method ini digunakan untuk mencetak menu yang dapat diakses berdasarkan jenis penggunaLoggedIn
    private static void showMenu() {
        if (penggunaLoggedIn instanceof Staf) {
            showMenuStaf();
        } else {
            showMenuAnggota();
        }
    }

    // Method ini digunakan untuk mencetak menu staf dari SistakaNG
    private static void showMenuStaf() {
        int command;
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println("================ Menu Staf ================\n");
            System.out.println("1. Tambah Anggota");
            System.out.println("2. Tambah Kategori");
            System.out.println("3. Tambah Buku");
            System.out.println("4. Hapus Buku");
            System.out.println("5. 3 Peringkat Pertama");
            System.out.println("6. Detail Anggota");
            System.out.println("7. Daftar Peminjam Buku");
            System.out.println("99. Logout");
            System.out.print("Masukkan pilihan menu: ");
            command = Integer.parseInt(input.nextLine());
            System.out.println();
            if (command == 1) { // Program Tambah Anggota
                System.out.println("================ Tambah Anggota ================");
                System.out.print("Tipe Anggota: ");
                String tipe = input.nextLine();

                // Menginisiasi daftarAnggota
                if (daftarAnggota == null) {
                    daftarAnggota = new Anggota[0];
                }
                
                if (tipe.equals("Mahasiswa")) {
                    System.out.print("Nama: ");
                    String nama = input.nextLine();
                    System.out.print("Program Studi (SIK/SSI/MIK/MTI/DIK): ");
                    String programStudi = input.nextLine();
                    System.out.print("Angkatan: ");
                    String angkatan = input.nextLine();
                    System.out.print("Tanggal Lahir (dd/mm/yyyy): ");
                    String tanggalLahir = input.nextLine();

                    Anggota[] newAnggota = new Anggota[daftarAnggota.length + 1];
                    if (daftarAnggota.length != 0) {
                        System.arraycopy(daftarAnggota, 0, newAnggota, 0, daftarAnggota.length);
                    }
                    // Menambahkan obj Mahasiswa ke dalam daftarAnggota
                    Mahasiswa mahasiswa = new Mahasiswa(nama, tanggalLahir, programStudi, angkatan);
                    if (!mahasiswa.getID().equals("Tidak valid")) {
                        newAnggota[newAnggota.length -1] = mahasiswa;
                        daftarAnggota= newAnggota.clone();
    
                        System.out.println("Berhasil menambahkan " + tipe + " dengan data:");
                        System.out.println(daftarAnggota[daftarAnggota.length-1].toString());
                        
                    } else {
                        System.out.println("Tidak dapat menambahkan anggota silahkan periksa kembali input anda!");
                    }

                } else if (tipe.equals("Dosen")) {
                    System.out.print("Nama: ");
                    String nama = input.nextLine();

                    Anggota[] newAnggota = new Anggota[daftarAnggota.length + 1];
                    if (daftarAnggota.length != 0) {
                        System.arraycopy(daftarAnggota, 0, newAnggota, 0, daftarAnggota.length);
                    }
                    // Menambahkan obj Dosen ke dalam daftarAnggota
                    newAnggota[newAnggota.length -1] = new Dosen(nama);
                    daftarAnggota= newAnggota.clone();

                    System.out.println("Berhasil menambahkan " + tipe + " dengan data:");
                    System.out.println(daftarAnggota[daftarAnggota.length-1].toString());

                } else {
                    System.out.println("Tipe Anggota " + tipe + " tidak valid!");
                }
            } else if (command == 2) { // Program Tambah Anggota
                System.out.println("---------- Tambah Kategori ----------");
                System.out.print("Nama Kategori: ");
                String namaKategori = input.nextLine();
                System.out.print("Poin: ");
                String point = input.nextLine();

                // Menambahkan Object kategori ke dalam daftarKategori
                if (daftarKategori == null) {
                    daftarKategori = new Kategori[1];
                    daftarKategori[daftarKategori.length -1] = new Kategori(namaKategori, Integer.parseInt(point));
                    System.out.println("Kategori " + namaKategori + " dengan poin " + point + " berhasil ditambahkan");

                } else {
                    if (!foundCategory(namaKategori)){
                        Kategori[] newCategories = new Kategori[daftarKategori.length + 1];
                        System.arraycopy(daftarKategori, 0, newCategories, 0, daftarKategori.length);
                        newCategories[newCategories.length -1] = new Kategori(namaKategori, Integer.parseInt(point));
                        daftarKategori = newCategories.clone();
                    
                        System.out.println("Kategori " + namaKategori + " dengan poin " + point + " berhasil ditambahkan");

                    } else {
                        System.out.println("Kategori " + daftarKategori[indexCategory].getNamaKategori() + " sudah pernah ditambahkan");
                    }
                }
            } else if (command == 3) { // Program tambah buku
                System.out.println("---------- Tambah Buku ----------");
                System.out.print("Judul: ");
                String judul = input.nextLine();
                System.out.print("Penulis: ");
                String penulis = input.nextLine();
                System.out.print("Penerbit: ");
                String penerbit = input.nextLine();
                System.out.print("Kategori: ");
                String kategori = input.nextLine();
                System.out.print("Stok: ");
                int stok = Integer.parseInt(input.nextLine());
                
                // Validasi
                if (!foundBook(judul, penulis)) {
                    if (foundCategory(kategori)) {
                        if (stok > 0) {
                            // Menambahkan Object buku ke dalam daftarBuku
                            if (daftarBuku == null) {
                                daftarBuku = new Buku[1];
                                daftarBuku[daftarBuku.length -1] = new Buku(judul, penulis, penerbit, stok, daftarKategori[indexCategory]);
                            } else {
                                Buku[] newBooks = new Buku[daftarBuku.length + 1];
                                System.arraycopy(daftarBuku, 0, newBooks, 0, daftarBuku.length);
                                newBooks[newBooks.length -1] = new Buku(judul, penulis, penerbit, stok, daftarKategori[indexCategory]);
                                daftarBuku = newBooks.clone();
                            }
                            System.out.println("Buku " + judul + " oleh " + penulis + " berhasil ditambahkan");
                        } else {
                            System.out.println("Stok harus lebih dari 0");
                        }
                    } else {
                        System.out.println("Kategori " + kategori + " tidak ditemukan");
                    }
                } else {
                    System.out.println("Buku " + daftarBuku[indexBook].getJudul() + " oleh " + daftarBuku[indexBook].getPenulis() + " sudah pernah ditambahkan");
                }
            } else if (command == 4) { // program Hapus buku
                System.out.println("---------- Hapus Buku ----------");
                System.out.print("Judul: ");
                String judul = input.nextLine();
                System.out.print("Penulis: ");
                String penulis = input.nextLine();

                // Validasi
                if (foundBook(judul, penulis)){
                    if (!foundBookLoan(judul)) {
                        System.out.println("Buku " + daftarBuku[indexBook].getJudul() + " oleh " + daftarBuku[indexBook].getPenulis() + " berhasil dihapus");

                        Buku[] newBooks = new Buku[daftarBuku.length -1];
                        System.arraycopy(daftarBuku, 0, newBooks, 0, indexBook); // copy the elements from index 0 until indexBook, from daftarBuku to newBooks
                        System.arraycopy(daftarBuku, indexBook + 1, newBooks, indexBook, daftarBuku.length - indexBook - 1); // copy the elements from indexBook till end, from daftarBuku to newBooks
                        daftarBuku = newBooks.clone();
                    } else {
                        System.out.println("Buku " + daftarBuku[indexBook].getJudul() + " oleh " + daftarBuku[indexBook].getPenulis() + " tidak dapat dihapus karena sedang dipinjam");
                    }
                } else {
                    System.out.println("Buku " + judul + " oleh " + penulis + " tidak ditemukan");
                }
            } else if (command == 5) { // Program Peringkat Anggota
                System.out.println("---------- Peringkat Anggota ----------");
                if (daftarAnggota != null) {
                    Anggota[] peringkat = daftarAnggota.clone();
                    Arrays.sort(peringkat); // Mengurutkan peringkat
                   
                    if (peringkat.length >= 3) {
                        for (int i = 0; i < 3; i++) {
                            System.out.println("—------------- " + (i + 1) + " —-------------");
                            System.out.println(peringkat[i].toString());
                        }
                    } else if (peringkat.length >= 1) {
                        for (int i = 0; i < peringkat.length; i++) {
                            System.out.println("—------------- " + (i + 1) + " —-------------");
                            System.out.println(peringkat[i].toString());
                        }
                    } 
                } else {
                    System.out.println("Belum ada anggota yang terdaftar pada sistem");
                }
            } else if (command == 6) {
                System.out.println("---------- Detail Anggota ----------");
                System.out.print("ID Anggota: ");
                String id = input.nextLine();

                // Validasi dan menjalankan method detail pada Class Anggota
                if (foundID(id)) {
                    daftarAnggota[indexMember].detail();
                } else {
                    System.out.println("Anggota dengan ID " + id + " tidak ditemukan");
                }
            } else if (command == 7) {
                System.out.println("---------- Daftar Peminjam Buku ----------");
                System.out.print("Judul: ");
                String judul = input.nextLine();
                System.out.print("Penulis: ");
                String penulis = input.nextLine();

                // Validasi dan cetak Daftar Peminjam
                if (foundBook(judul, penulis)) {
                    System.out.println(daftarBuku[indexBook].toString());
                    daftarBuku[indexBook].DaftarPeminjam();
                } else {
                    System.out.println("Buku " + judul + " oleh " + penulis + " tidak ditemukan");
                }
            } else if (command == 99) {
                System.out.println("Terima kasih telah menggunakan SistakaNG!");
                hasChosenExit = true;
            } else {
                System.out.println("Menu tidak dikenal!");
            }
            System.out.println();
        }
    }

    // Method ini digunakan untuk mencetak menu anggota dari SistakaNG
    private static void showMenuAnggota() {
        int command;
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println("================ Menu Anggota ================\n");
            System.out.println("1. Peminjaman");
            System.out.println("2. Pengembalian");
            System.out.println("3. Pembayaran Denda");
            System.out.println("4. Detail Anggota");
            System.out.println("99. Logout");
            System.out.print("Masukkan pilihan menu: ");
            command = Integer.parseInt(input.nextLine());
            System.out.println();
            if (command == 1) {
                System.out.println("---------- Peminjaman Buku ----------");
                System.out.print("Judul Buku: ");
                String judul = input.nextLine();
                System.out.print("Penulis Buku: ");
                String penulis = input.nextLine();
                System.out.print("Tanggal Peminjaman: ");
                String tanggalPinjam = input.nextLine();

                // Validasi
                if(foundBook(judul, penulis)) {
                    if(daftarBuku[indexBook].getStok() >= 1) {
                        if (penggunaLoggedIn instanceof Mahasiswa) { // Memanggil method pinjam
                            System.out.println(((Mahasiswa)penggunaLoggedIn).pinjam(daftarBuku[indexBook], tanggalPinjam));
                        } else {
                            System.out.println(((Dosen)penggunaLoggedIn).pinjam(daftarBuku[indexBook], tanggalPinjam));
                        }
                    } else {
                        System.out.println("Buku " + daftarBuku[indexBook].getJudul() + " oleh " + daftarBuku[indexBook].getPenulis() + " tidak tersedia");
                    }
                } else {
                    System.out.println("Buku " + judul + " oleh " + penulis + " tidak ditemukan");
                }

            } else if (command == 2) {
                System.out.println("---------- Pengembalian Buku ----------");
                System.out.print("Judul Buku: ");
                String judul = input.nextLine();
                System.out.print("Penulis Buku: ");
                String penulis = input.nextLine();
                System.out.print("Tanggal Pengembalian: ");
                String tanggalKembali = input.nextLine();

                // Validasi
                if (foundBook(judul, penulis)) {
                    if (penggunaLoggedIn instanceof Mahasiswa) { // Memanggil method kembalu
                        System.out.println(((Mahasiswa)penggunaLoggedIn).kembali(daftarBuku[indexBook], tanggalKembali));
                    } else {
                        System.out.println(((Dosen)penggunaLoggedIn).kembali(daftarBuku[indexBook], tanggalKembali));
                    }
                } else {
                    System.out.println("Buku " + judul + " oleh " + penulis + " tidak ditemukan");
                }

            } else if (command == 3) {
                System.out.println("---------- Pembayaran Denda ----------");
                System.out.print("Jumlah: ");
                long jumlah = Long.parseLong(input.nextLine());

                System.out.println(((Anggota)penggunaLoggedIn).bayarDenda(jumlah));
             
            } else if (command == 4) {
                daftarAnggota[indexMember].detail();
                
            } else if (command == 99) {
                System.out.println("Terima kasih telah menggunakan SistakaNG!");
                hasChosenExit = true;
            } else {
                System.out.println("Menu tidak dikenal!");
            }
            System.out.println();
        }
    }

    /* 
     * Method untuk mencari kategori dalam daftarKategoru, return true jika ditemukan
     */ 
    public static boolean foundCategory(String kategori) {
        boolean foundCategory = false;
        if (daftarKategori != null) {
            for (int i = 0; i < daftarKategori.length; i++){
                if (daftarKategori[i].getNamaKategori().equalsIgnoreCase(kategori)) {
                    foundCategory = true;
                    setIndexCategory(i);
                    break;
                }
            } 
        }
        return foundCategory; 
    }

    // Setter
    public static void setIndexCategory(int index) {
        indexCategory = index;
    }

    /* 
     * Method untuk mencari ID dalam daftarAnggota, return true jika ditemukan
     */ 
    public static boolean foundID(String idAnggota) {
        boolean foundID = false;
        if (daftarAnggota != null) {
            for (int i = 0; i < daftarAnggota.length; i++) {
                if (daftarAnggota[i].getID().equals(idAnggota)) {
                    foundID = true;
                    setIndexMember(i);
                    break;
                }    
            }
        }
        return foundID;
    }

    // Setter
    public static void setIndexMember(int index) {
        indexMember = index;
    }

    /* 
     * Method untuk mencari buku dalam daftarBuku, return true jika ditemukan
     */ 
    public static boolean foundBook(String judul, String penulis) {
        boolean foundBook = false;
        if (daftarBuku != null) {
            for(int i = 0; i < daftarBuku.length; i++) {
                if (daftarBuku[i].getJudul().equalsIgnoreCase(judul) &&
                    daftarBuku[i].getPenulis().equalsIgnoreCase(penulis)) {
                        foundBook = true;
                        setIndexBook(i);
                        break;
                    }
            }
        }
        return foundBook;
    }

    // Setter
    public static void setIndexBook(int index) {
        indexBook = index;
    }

    /* 
     * Method untuk mencari buku yang dipinjam dalam daftarPeminjaman yang ada dalam Class Anggota,
     * return true jika ditemukan
     */ 
    public static boolean foundBookLoan(String judul) {
        boolean foundBookLoan = false;

        for (Anggota anggota : daftarAnggota) {
            Peminjaman[] daftarPinjamAnggota = anggota.getDaftarPeminjaman();
            if (daftarPinjamAnggota != null) {
                for (int k = 0; k < daftarPinjamAnggota.length; k++) {
                    if (daftarPinjamAnggota[k].getBuku().getJudul().equals(judul) && daftarPinjamAnggota[k].getTanggalKembali().equals("-")) {
                        foundBookLoan = true;
                        setIndexBookLoan(k);
                        break;
                    }
                }
            }

        }
        return foundBookLoan;
    }

    // Setter
    public static void setIndexBookLoan(int index) {
        indexBookLoan = index;
    }

    // Getter
    public static Anggota[] getDaftarAnggota() {
        return daftarAnggota;
    }

    public static Staf[] getDaftarStaf() {
        return daftarStaf;
    }
}
