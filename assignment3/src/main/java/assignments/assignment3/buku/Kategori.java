package assignments.assignment3.buku;

public class Kategori {
   // Attribute kelas Kategori
    private String nama;
    private int poin;

    // Constructor kelas Kategori
    public Kategori(String nama, int poin) {
        this.nama = nama;
        this.poin = poin;
    }

    @Override
    public String toString() {
        return "Kategori: " + nama +
                "\nPoin: " + poin;
    }

    // Getter method
    public String getNamaKategori() {
        return this.nama;
    }
    public int getPoin() {
        return this.poin;
    }
}
