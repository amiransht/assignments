package assignments.assignment3.buku;

import assignments.assignment3.pengguna.Anggota;
import assignments.assignment3.pengguna.CanBorrow;

public class Buku {
    //Atribut kelas Buku
    private String judul;
    private String penulis;
    private String penerbit;
    private int stokAwal;
    private int stok;
    private Kategori kategori;
    private CanBorrow[] daftarPeminjam;

    // Constructor kelas Buku
    public Buku(String judul, String penulis, String penerbit, int stokAwal, Kategori kategori) {
        this.judul = judul;
        this.penulis = penulis;
        this.penerbit = penerbit;
        this.stokAwal = stokAwal;
        this.kategori = kategori;
        this.stok = stokAwal;
        this.daftarPeminjam = new CanBorrow[0];
    }

    @Override
    public String toString() {
        return "Judul Buku: " + judul +
                "\nPenulis Buku: " + penulis +
                "\nPenerbit Buku: " + penerbit + "\n" +
                kategori.toString();
    }
    // Method untuk mencetak seluruh Daftar Peminjam Buku ini
    public void DaftarPeminjam() {
        System.out.println("---------- Daftar Peminjam ----------");
        if (daftarPeminjam.length != 0) {
            for (int i = 0; i < daftarPeminjam.length; i++) {
                System.out.println("—------------- " + (i + 1) + " —-------------");
                System.out.println(daftarPeminjam[i].toString());
            }
        } else {
            System.out.println("Belum ada anggota yang meminjam buku " + judul);
        }
    }

    // Method untuk menambahkan Anggota ke daftar Peminjam
    public void tambahPeminjam(Anggota anggota) {
        CanBorrow[] temp = new CanBorrow[daftarPeminjam.length + 1];

        if (daftarPeminjam.length != 0) {
            System.arraycopy(daftarPeminjam, 0, temp, 0, daftarPeminjam.length);
        }
        temp[temp.length-1] = anggota;
        daftarPeminjam = temp.clone();

    }

    // Getter Method
    public String getJudul() {
        return this.judul;
    }
    public String getPenulis() {
        return this.penulis;
    }
    public String getPenerbit() {
        return this.penerbit;
    }
    public Kategori getKategori() {
        return this.kategori;
    }
    public int getStok() {
        return this.stok;
    }

    // Setter Method
    public void setStok(int currentStok) {
        this.stok = currentStok;
    }

}