package assignments.assignment3.buku;

import assignments.assignment3.pengguna.Anggota;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Peminjaman {
    // Attribute kelas Peminjaman
    private final long DENDA_PER_HARI = 3000;
    private Anggota anggota;
    private Buku buku;
    private String tanggalPeminjaman;
    private String tanggalPengembalian = "-";
    private long denda = 0;

    // Constructor kelas Peminjaman
    public Peminjaman(Anggota anggota, Buku buku, String tanggalPeminjaman) {
        this.anggota = anggota;
        this.buku = buku;
        this.tanggalPeminjaman = tanggalPeminjaman;
        buku.tambahPeminjam(anggota);
    }

    @Override
    public String toString() {
        return buku.toString() +
                "\nTanggal Peminjaman: " + tanggalPeminjaman +
                "\nTanggal Pengembalian: " + tanggalPengembalian +
                "\nDenda: Rp" + denda;
    }

    // Method untuk melakukan operasi pengembalian buku dan menghitung denda
    public void kembalikanBuku(String tanggalPengembalian) {
        this.tanggalPengembalian = tanggalPengembalian;
        try {
            denda = hitungDenda();
        } catch (Exception e) {
            System.out.println();
        }
       
    }

    // Method untuk menghitung total denda setelah buku dikembalikan
    public long hitungDenda() throws Exception {
        Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(tanggalPeminjaman); 
        Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(tanggalPengembalian); 
                
        long diffInMillies = date2.getTime() - date1.getTime();
        long diffDate = TimeUnit.DAYS.convert(diffInMillies,TimeUnit.MILLISECONDS);

        if (diffDate > 7) {
            denda = (diffDate-7) * DENDA_PER_HARI;
        }
        return denda;
    }

    // Getter Method
    public Buku getBuku() {
        return this.buku;
    }

    public String getTanggalKembali() {
        return this.tanggalPengembalian;
    }

    public String getTanggalPinjam() {
        return this.tanggalPeminjaman;
    }

    public long getDenda() {
        return this.denda;
    }
}
