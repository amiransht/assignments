package assignments.assignment3.pengguna;

import assignments.assignment3.buku.Buku;
import assignments.assignment3.buku.Kategori;

public interface CanBorrow {
    // Method kelas interface CanBorrow
    public abstract String pinjam(Buku buku, String tanggalPeminjaman);

    public abstract String kembali(Buku buku, String tanggalPengembalian);
}
