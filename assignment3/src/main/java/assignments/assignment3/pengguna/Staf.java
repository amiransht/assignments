package assignments.assignment3.pengguna;

import assignments.assignment3.SistakaNG;

public class Staf extends Pengguna {
    // Attribute kelas Staf
    private int jumlahStaf = 0;

    // Constructor kelas Staf
    public Staf(String nama) {
        super(nama);
        setID(generateId());
    }

    @Override
    public String toString() {
        return "ID Staf: " + super.getID() +
                "\nNama Staf: " + super.getNama();
    }

    @Override // Method untuk membuat ID Staf
    protected String generateId() {
        Staf[] daftarStaf = SistakaNG.getDaftarStaf();
        for (int i = 0; i < daftarStaf.length + 1; i++) {
            jumlahStaf++;
        }
        return "STAF-" + (jumlahStaf);
       
        
    }


}
