package assignments.assignment3.pengguna;

import assignments.assignment3.SistakaNG;
import assignments.assignment3.buku.Buku;
import assignments.assignment3.buku.Peminjaman;

public class Dosen extends Anggota{
    // Attribute Kelas Dosen
    private int jumlahDosen = 0;
    public final int BATAS_JUMLAH_PEMINJAMAN_BUKU = 5;
    public final long BATAS_MAKSIMAL_DENDA = 10000;

    // Constructor kelas Dosen
    public Dosen(String nama) {
        super(nama);
        setID(generateId());
    }

    @Override // Method untuk mengimplementasikan pinjam Buku
    public String pinjam(Buku buku, String tanggalPeminjaman) {
        if (daftarPeminjaman == null) {
            daftarPeminjaman = new Peminjaman[1];
            daftarPeminjaman[0] = new Peminjaman(this, buku, tanggalPeminjaman);
            return getNama() + " berhasil meminjam Buku " + buku.getJudul() + "!";
        }

        if (jumlahPinjam() < BATAS_JUMLAH_PEMINJAMAN_BUKU) {
            if (denda <= BATAS_MAKSIMAL_DENDA) {
                if(!foundBookLoan(buku)) {
                    // Mengurangi stok Buku
                    buku.setStok(buku.getStok()-1);

                    Peminjaman[] temp = new Peminjaman[getDaftarPeminjaman().length+1];
                    if (getDaftarPeminjaman().length != 0) {
                        for (int i = 0; i < getDaftarPeminjaman().length; i++) {
                            temp[i] = getDaftarPeminjaman()[i];
                        }
                    }
                    // Menambahkan obj Peminjaman ke daftarPeminjaman
                    temp[temp.length-1] = new Peminjaman(this, buku, tanggalPeminjaman);
                    daftarPeminjaman = temp.clone();

                    return getNama() + " berhasil meminjam Buku " + buku.getJudul() + "!";
                } else {
                    return "Buku " + buku.getJudul() + " oleh " + buku.getPenulis() + " sedang dipinjam";
                }
            } else {
                return "Denda lebih dari Rp10000";
            }
        } else {
            return "Jumlah buku yang sedang dipinjam sudah mencapai batas maksimal";
        }
    }

    @Override // Method untuk membuat ID Dosen
    protected String generateId() {
        Anggota[] daftarAnggota = SistakaNG.getDaftarAnggota() ;
        if (daftarAnggota.length != 0) {
            for (Anggota anggota : daftarAnggota) {
                if (anggota instanceof Dosen) {
                    jumlahDosen++;
                }
            }
        } 
       
        return "DOSEN-" + (++jumlahDosen);
       
    }

    


    


}
