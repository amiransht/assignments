package assignments.assignment3.pengguna;

import assignments.assignment3.buku.Buku;
import assignments.assignment3.buku.Peminjaman;

public abstract class Anggota extends Pengguna implements Comparable<Anggota>, CanBorrow{
    // Attribute kelas Anggota
    protected long denda = 0;
    protected int poin = 0;
    protected Peminjaman[] daftarPeminjaman;
    private int indexBookLoan = 0;

    // Constructor kelas Anggota
    public Anggota(String nama) {
        super(nama);
    }

    @Override
    public String toString() {
        return "ID Anggota: " + super.getID() +
                "\nNama Anggota: " + super.getNama() +
                "\nTotal poin: " + poin +
                "\nDenda: Rp" + denda;
    }

    @Override // Method override untuk membandingkan kelas Anggota berdasarkan poin dan alphabet nama
    public int compareTo(Anggota other) {
        if (this.poin > other.poin) {
            return -1;
        } else if (this.poin < other.poin) {
            return 1;
        } else {
            if (this.getNama().compareToIgnoreCase(other.getNama()) > 0) {
                return 1;
            } else if (this.getNama().compareToIgnoreCase(other.getNama()) < 0) {
                return -1;
            } else {
                return 0;
            }
        }
    }
    
    @Override
    public abstract String pinjam(Buku buku, String tanggalPeminjaman);

    @Override // Method untuk mengimplementasikan pengembalian buku
    public String kembali(Buku buku, String tanggalPengembalian){
        if (foundBookLoan(buku)) {
            daftarPeminjaman[indexBookLoan].kembalikanBuku(tanggalPengembalian);
            buku.setStok(buku.getStok() + 1);
            this.poin += daftarPeminjaman[indexBookLoan].getBuku().getKategori().getPoin();
            this.denda += daftarPeminjaman[indexBookLoan].getDenda();
            return "Buku " + buku.getJudul() + " berhasil dikembalikan oleh " + getNama() + " dengan denda RP" + daftarPeminjaman[indexBookLoan].getDenda() + "!";
        
        } else {
            return "Buku " + buku.getJudul() + " tidak sedang dipinjam oleh " + getNama();
        }
    
    }
    // Method untuk mencetak detail Anggota
    public void detail(){
        System.out.println(this);
        System.out.println("Riwayat Peminjaman Buku :");
        if (daftarPeminjaman != null) {
            for (int i = 0; i < daftarPeminjaman.length; i++) {
                System.out.println("—------------- " + (i + 1) + " —-------------");
                System.out.println("Judul Buku: " + daftarPeminjaman[i].getBuku().getJudul());
                System.out.println("Penulis Buku: " + daftarPeminjaman[i].getBuku().getPenulis());
                System.out.println("Penerbit Buku: " + daftarPeminjaman[i].getBuku().getPenerbit());
                System.out.println("Kategori: " + daftarPeminjaman[i].getBuku().getKategori().getNamaKategori());
                System.out.println("Poin: " + daftarPeminjaman[i].getBuku().getKategori().getPoin());
                System.out.println("Tanggal Peminjaman: " + daftarPeminjaman[i].getTanggalPinjam());
                System.out.println("Tanggal Pengembalian: " + daftarPeminjaman[i].getTanggalKembali());
                System.out.println("Denda: Rp " + daftarPeminjaman[i].getDenda()); 
            }
        } else {
            System.out.println("Belum pernah meminjam buku");
        }
    }

    // Method untuk mengimplementasikan pembayaran denda Anggota
    public String bayarDenda(long jumlahBayar) {
        if (denda == 0) {
            return getNama() + " tidak memiliki denda";
        } else if (jumlahBayar < denda) {
            denda -= jumlahBayar;
            return getNama() + " berhasil membayar denda sebesar Rp" + jumlahBayar + "\nSisa denda saat ini: Rp" + denda;
        } else {
            long kembalian = jumlahBayar - denda;
            denda = 0;
            return getNama() + " berhasil membayar lunas denda" + "\nJumlah kembalian: Rp" + kembalian;
        }
    }

    // Getter Method
    public Peminjaman[] getDaftarPeminjaman() {
        return this.daftarPeminjaman;
    }

    // Method untuk menghitung jumlah Buku yang sedang dipinjam
    public int jumlahPinjam() {
        int jumlah = 0;
        for (Peminjaman peminjaman : daftarPeminjaman) {
            if (peminjaman.getTanggalKembali().equals("-")) {
                jumlah++;
            }
        }
        return jumlah;
    }

    // Method untuk mencari buku di dalam daftarPeminajman, return true jika ditemukan
    public boolean foundBookLoan(Buku buku) {
        boolean found = false;
        for (int i = 0; i < daftarPeminjaman.length; i++) {
            if(daftarPeminjaman[i].getBuku().equals(buku)) {
                found = true;
                setIndexBookLoan(i);
                break;
            }
        }
        return found;
    }

    public void setIndexBookLoan(int index) {
        this.indexBookLoan = index;
    }





}