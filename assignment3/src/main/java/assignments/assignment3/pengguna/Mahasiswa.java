package assignments.assignment3.pengguna;

import assignments.assignment3.IdGenerator;
import assignments.assignment3.buku.Buku;
import assignments.assignment3.buku.Peminjaman;

public class Mahasiswa extends Anggota{
    // Attribute kelas Mahasiswa
    public final int BATAS_JUMLAH_PEMINJAMAN_BUKU = 3;
    public final long BATAS_MAKSIMAL_DENDA = 5000;
    private String tanggalLahir;
    private String programStudi;
    private String angkatan;

    // Constructor kelas Mahasiswa
    public Mahasiswa(String nama, String tanggalLahir, String programStudi, String angkatan) {
        super(nama);
        this.programStudi = programStudi;
        this.angkatan = angkatan;
        this.tanggalLahir = tanggalLahir;
        setID(generateId());
    }

    @Override // Method untuk membuat ID Mahasiswa
    protected String generateId() {
        return IdGenerator.generateId(programStudi, angkatan, tanggalLahir);
    }

    @Override // Method untuk mengimplementasikan pinjam buku
    public String pinjam(Buku buku, String tanggalPeminjaman) {
        if (daftarPeminjaman == null) {
            daftarPeminjaman = new Peminjaman[1];
            daftarPeminjaman[0] = new Peminjaman(this, buku, tanggalPeminjaman);
            return getNama() + " berhasil meminjam Buku " + buku.getJudul() + "!";
        }
   
        if (jumlahPinjam() < BATAS_JUMLAH_PEMINJAMAN_BUKU) {
            if (denda <= BATAS_MAKSIMAL_DENDA) {
                if(!foundBookLoan(buku)) {
                    // Mengurangi stok buku
                    buku.setStok(buku.getStok()-1);
                    
                    Peminjaman[] temp = new Peminjaman[getDaftarPeminjaman().length+1];
                    if (getDaftarPeminjaman().length != 0) {
                        for (int i = 0; i < getDaftarPeminjaman().length; i++) {
                            temp[i] = getDaftarPeminjaman()[i];
                        }
                    }
                    // Menambahkan obj Peminjaman ke daftarPeminjaman
                    temp[temp.length-1] = new Peminjaman(this, buku, tanggalPeminjaman);
                    daftarPeminjaman = temp.clone();
                   
                    return getNama() + " berhasil meminjam Buku " + buku.getJudul() + "!";

                } else {
                    return "Buku " + buku.getJudul() + " oleh " + buku.getPenulis() + " sedang dipinjam";
                }
            } else {
                return "Denda lebih dari Rp5000";
            }
        } else {
            return "Jumlah buku yang sedang dipinjam sudah mencapai batas maksimal";
        }
    }

    
  
}

   
