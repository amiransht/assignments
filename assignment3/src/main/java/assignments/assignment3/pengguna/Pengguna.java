package assignments.assignment3.pengguna;

public abstract class Pengguna {
    // Attribute kelas Pengguna
    private String id;
    private String nama;

    // Constructor kelas Pengguna
    public Pengguna(String nama) {
        this.nama = nama;
    }

    protected abstract String generateId();
    public abstract String toString();

    // Getter Method
    public String getNama() {
        return this.nama;
    }

    public String getID() {
        return this.id;
    }

    // Setter Method
    public void setID(String id) {
        this.id = id;
    }

}
